
function drawLine(points,id,size,zIndex,color){
	//console.log(points);
	if(document.getElementById(id)){
		var line = document.getElementById(id);
		if(cam.pos[1]<-10){
			updateAttributes(line,{
				x1:points[0][0],
				y1:points[0][1],
				x2:points[1][0],
				y2:points[1][1],
				stroke:color,
				'stroke-width':1+'px',
				'stroke-linecap':'round',
			});
		}else{
			updateAttributes(line,{
				x1:points[0][0],
				y1:points[0][1],
				x2:points[1][0],
				y2:points[1][1],
				stroke:"none",
				'stroke-width':1+'px',
				'stroke-linecap':'round',
			});
		}
		updateCSS(line.parentNode,{
			zIndex:zIndex-1
		});
		
		updateCSS(line,{
			zIndex:zIndex-1
		});
	}else{
		var svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
		updateCSS(svg,{
			width:'100%',
			height:'100%',
		});
		var line = document.createElementNS('http://www.w3.org/2000/svg', 'line');
		line.id = id
		svg.appendChild(line);
		document.body.appendChild(svg);
	}

}

function w(){
	return window.innerWidth;
}
function h(){
	return window.innerHeight;
}
function cx(){
	return Math.floor(w()/2);
}
function cy(){
	return Math.floor(h()/2);
}

Keys = function(){
}

Keys.prototype.newKey = function(key){
	this[key]=true;
}

keys = new Keys();


function rotate2d(pos,rad){
	var x = pos[0];
	var y = pos[1];

	var s = Math.sin(rad);
	var c = Math.cos(rad);

	return [x*c - y*s,y*c + x*s]
}

Cam = function(options){
	define(this,options,{
		pos:[0,0,0],
		scale:[0,0,1],
		rot:[0,0]
	});
}

Cam.prototype.events = function(event) {
	if(event.type=='mousemove'){
		var x = event.movementX;
		var y = event.movementY;
		x/=cx(); 
		y/=cy();
		this.rot[0]-=y*this.scale[2]; this.rot[1]-=x*this.scale[2];
	}
};

Cam.prototype.update = function(s,key) {



	var x = s*Math.sin(this.rot[1])*this.scale[2];
	var y = s*Math.cos(this.rot[1])*this.scale[2];
	var xZ = s*Math.sin(this.rot[0])*this.scale[2];
	var yZ = s*Math.cos(this.rot[0])*this.scale[2];


//console.log(this.rot[0],xZ);

if (key['a']){ this.pos[1]-=s*this.scale[2]};
if (key['e']){ this.pos[1]+=s*this.scale[2]};

if (key['z']){ this.pos[0]+=x; this.pos[2]+=y ;/* this.pos[1]+=xZ*/};
if (key['s']){ this.pos[0]-=x; this.pos[2]-=y ; /* this.pos[1]-=xZ*/};

if (key['q']){ this.pos[0]-=y; this.pos[2]+=x ; /*this.pos[1]-=yZ*/};
if (key['d']){ this.pos[0]+=y; this.pos[2]-=x ; /*this.pos[1]+=yZ*/};

if (key['+']){ this.scale[2]/=1+s/3;};
if (key['-']){ this.scale[2]*=1+s/3};

if (key['ArrowUp']){ this.scale[1]+=s*50*this.scale[2];};
if (key['ArrowDown']){ this.scale[1]-=s*50*this.scale[2];};

if (key['ArrowLeft']){ this.scale[0]+=s*50*this.scale[2];};
if (key['ArrowRight']){ this.scale[0]-=s*50*this.scale[2];};

if (key['Backspace']){ 
	if(this.scale[2]>1){
		this.scale[2]/=1+s/3;
	}
	if(this.scale[2]<1){
		this.scale[2]*=1+s/3;
	}
	this.scale[0]-=this.scale[0]*0.1;
	this.scale[1]-=this.scale[1]*0.1
};

}

Cam.prototype.spatializeCoord = function(coord){

	var x=coord[0], y=coord[1], z=coord[2];

	x-=this.pos[0];
	y-=this.pos[1];
	z-=this.pos[2];

	hRot = rotate2d([x,z],this.rot[1])
	vRot = rotate2d([y,hRot[1]],this.rot[0])

	var f = cx()/vRot[1];
	x = hRot[0]*f;
	y = vRot[0]*f;

	dist=Math.sqrt(Math.pow(this.pos[0]-coord[0],2)+Math.pow(this.pos[1]-coord[1],2)+Math.pow(this.pos[2]-coord[2],2));
	x=this.scale[0]-(this.scale[0]-x+this.scale[0]*this.scale[2])/this.scale[2];
	y=this.scale[1]-(this.scale[1]-y+this.scale[1]*this.scale[2])/this.scale[2];

	return{
		point:[cx()+x,cy()+y],
		z:vRot[1],
		dist:dist
	}

}

Cam.prototype.spatializeNode = function(node){

	var x=node.coord[0], y=node.coord[1], z=node.coord[2];

	x-=this.pos[0];
	y-=this.pos[1];
	z-=this.pos[2];

	hRot = rotate2d([x,z],this.rot[1])
	vRot = rotate2d([y,hRot[1]],this.rot[0])

	var f = cx()/vRot[1];
	x = hRot[0]*f;
	y = vRot[0]*f;

	var zIndex = -parseInt(vRot[1]*nodes.length);
	dist=Math.sqrt(Math.pow(this.pos[0]-node.coord[0],2)+Math.pow(this.pos[1]-node.coord[1],2)+Math.pow(this.pos[2]-node.coord[2],2));
	size=w()/100*(node.size/dist);
	x=this.scale[0]-(this.scale[0]-x+this.scale[0]*this.scale[2])/this.scale[2];
	y=this.scale[1]-(this.scale[1]-y+this.scale[1]*this.scale[2])/this.scale[2];

	size/=this.scale[2];

	node.display={x:cx()+x,y:cy()+y,z:vRot[1],dist:dist,size:size,zIndex:zIndex};

}

var cam = new Cam({pos:[0,0,-5]});

var nodes=[];

Node = function(options){
	define(this,options,{
		cam:cam,
		dom:document.createElement('div'),
		display:{x:0,y:0,z:0,dist:0,size:0,zIndex:0},
		id:nodes.length,
		coord:[0,0,0],
		size:100,
		image:undefined,
		descr:'',
	});
	updateAttributes(this.dom,{
		'contenteditable':true,
	})
	this.dom.textContent=this.descr;

	nodes.push(this);
}

var viewMode=0;

Node.prototype.updateDom = function(){

	this.cam.spatializeNode(this);

	updateCSS(this.dom,{
		marginLeft:this.display.x-this.display.size/2+'px',
		marginTop:this.display.y-this.display.size/2+'px',
		fontSize:this.display.size/2+'px',
		zIndex:this.display.zIndex,
		height:this.display.size+'px',
		width:this.display.size/4.5*this.descr.length+'px',
		'background-color':'white',
		outline:'solid',
		'outline-width':'1px',
	});


}


console.log(nodes);
var reach=[false,false,false,false,false,false,false,false];
var reachPoints=[[0,0,-5,0,0,0,0,0]];
var reachIndex=0;

window.repeat = setInterval(function(){
/*
	for(var i = 0; i< 41 ; i++ ){
		drawLine([cam.spatializeCoord([-5,0,-6+i]).point,cam.spatializeCoord([12,0,-6+i]).point],i,00,-99999999,'silver');
	}

	for(var i = 0; i< 18 ; i++ ){
		drawLine([cam.spatializeCoord([-5+i,0,-6]).point,cam.spatializeCoord([-5+i,0,34]).point],i+'x',10,-99999999,'silver');
	}
	
	drawLine([cam.spatializeCoord([-0.5,0,5]).point,cam.spatializeCoord([-3.5,0,9]).point],'link1',00,-99999999,'rgb(255,128,255)');
	drawLine([cam.spatializeCoord([-3.5,0,15]).point,cam.spatializeCoord([-0.5,0,26]).point],'link2',00,-99999999,'rgb(255,128,255)');
	drawLine([cam.spatializeCoord([-0.5,0,5]).point,cam.spatializeCoord([-0.5,0,26]).point],'link3',00,-99999999,'rgb(128,128,255)');
	drawLine([cam.spatializeCoord([-0.5,0,5]).point,cam.spatializeCoord([-0.5,0,26]).point],'link4',00,-99999999,'rgb(128,255,128)');
	drawLine([cam.spatializeCoord([-0.5,0,5]).point,cam.spatializeCoord([-0.5,0,26]).point],'link4',00,-99999999,'rgb(128,255,128)');
	*/
	for (var i in cam.pos){
		if(reach[i]){
			if(cam.pos[i]!=reachPoints[reachIndex][i]){

				cam.pos[i]+=(reachPoints[reachIndex][i]-cam.pos[i])/75;

				if(reachPoints[reachIndex][i]-cam.pos[i]>0){
					if(reachPoints[reachIndex][i]-cam.pos[i]<0.01*cam.scale[2]){
						cam.pos[i]=reachPoints[reachIndex][i];
						reach[i]=false;

					}
				}else{
					if(reachPoints[reachIndex][i]-cam.pos[i]>-0.01*cam.scale[2]){
						cam.pos[i]=reachPoints[reachIndex][i];
						reach[i]=false;

					}
				}
			}
		}
	}



	for (var i in cam.rot){
		var index=parseInt(i)+3;

		if(reach[index]){
			if(cam.rot[i]!=reachPoints[reachIndex][index]){

				cam.rot[i]+=(reachPoints[reachIndex][index]-cam.rot[i])/75;

				if(reachPoints[reachIndex][index]-cam.rot[i]>0){
					if(reachPoints[reachIndex][index]-cam.rot[i]<0.01*cam.scale[2]){
						cam.rot[i]=reachPoints[reachIndex][index];
						reach[index]=false;
					}
				}else{
					if(reachPoints[reachIndex][index]-cam.rot[i]>-0.01*cam.scale[2]){
						cam.rot[i]=reachPoints[reachIndex][index];
						reach[index]=false;
					}
				}

			}
			
		}
	}

	for (var i in cam.scale){
		var index=parseInt(i)+5;

		if(reach[index]){
			if(cam.scale[i]!=reachPoints[reachIndex][index]){

				cam.scale[i]+=(reachPoints[reachIndex][index]-cam.scale[i])/75;

				if(reachPoints[reachIndex][index]-cam.scale[i]>0){
					if(reachPoints[reachIndex][index]-cam.scale[i]<0.01*cam.scale[2]){
						cam.scale[i]=reachPoints[reachIndex][index];
						reach[index]=false;
					}
				}else{
					if(reachPoints[reachIndex][index]-cam.scale[i]>-0.01*cam.scale[2]){
						cam.scale[i]=reachPoints[reachIndex][index];
						reach[index]=false;
					}
				}

			}
			
		}
	}
	

	cam.update(0.05,keys);


	for(var i in nodes){

		nodes[i].updateDom();
	}

	

	//scroll
/*
	if(scroll>0){
console.log(scroll);
		if(wheelDelta>0){
			cam.update(0.05,{s:true,z:false});
			//cam.pos[2]-=scroll/100;
		}
		if(wheelDelta<0){
			cam.update(0.05,{z:true,s:false});
			//cam.pos[2]+=scroll/75;
		}
		scroll=0;
	}
	*/
	//console.log(cam.pos);

},17)

document.onkeydown = function(e){
	if(keys[e.key]==undefined){
		keys.newKey(e.key);
	}else{
		keys[e.key]=true;
	}



}

document.onkeyup = function(e){

	if(keys[e.key]==undefined){
		keys.newKey(e.key);
	}else{
		keys[e.key]=false;
	}

	if(e.keyCode==32){
		cam.pos[1]=-25;
		cam.scale[2]=1;
		move([0,0,-1,0,0,0,0,1.5]);
	}


}
/*
	if(window['turngauche']!=undefined){
		clearInterval(window['turngauche']);
		delete window['turngauche'];
	}
	*/

	function moveButton(start,end,text){
		var name ='moveButton'+Math.random();
		window[name]=setInterval(function(){
			
			if(cam.pos[2]>start[2]-1 && cam.pos[2]<start[2]+0.06125
				&& cam.pos[1]>start[1]-0.5 && cam.pos[1]<start[1]+0.5
				&& cam.pos[0]>start[0]-0.5 && cam.pos[0]<start[0]+0.5){
				if(document.getElementById(name)==null){
					var button = document.createElement('button');
					button.id=name;
					button.textContent='vers : '+text;
					button.onmousedown=function(){
						move(end);
					}
					document.getElementById('buttons').appendChild(button);
				}



			}else{
				
				if(document.getElementById(name)!=null){
					document.getElementById(name).parentNode.removeChild(document.getElementById(name));
				}
			}
		},17);

	}

	function textArea(where,textContent,className){

		var name ='textArea'+Math.random();

		window[name]=setInterval(function(){
			
			if(cam.pos[2]>where[2]-1 && cam.pos[2]<where[2]
				&& cam.pos[1]>where[1]-0.5 && cam.pos[1]<where[1]+0.5
				&& cam.pos[0]>where[0]-0.5 && cam.pos[0]<where[0]+0.5){
				if(document.getElementById(name)==null){
					var text = document.createElement('div');
					text.id=name;
					text.className=className;
					console.log(text);
					text.textContent=textContent;
					document.getElementById('text').appendChild(text);
				}



			}else{
				
				if(document.getElementById(name)!=null){
					document.getElementById(name).parentNode.removeChild(document.getElementById(name));
				}
			}
		},17);

	}


	function move(pos){

		for(var i in pos){
			reachPoints[0][i]=pos[i];
			reach[i]=true;
		}

	}

	function turn(direction,degree){
		var radian = 0.785398*2;
		if(degree!=undefined){
			radian=degree*0.01745328888888888888888888888889;
		}
		if(direction=='gauche'){
			radian *= -1;
			reachPoints[0][4]+=radian;
			reach[4]=true;
		}
		if(direction=='droite'){
			radian *= 1;
			reachPoints[0][4]+=radian;
			reach[4]=true;
		}
		if(direction=='bas'){
			radian *= 1;
			reachPoints[0][3]+=radian;
			reach[3]=true;
		}
		if(direction=='haut'){
			radian *= -1;
			reachPoints[0][3]+=radian;
			reach[3]=true;
		}

	}

	var mousedown = false;

	var scroll=0;

	var wheelDelta=0;

	document.onmousewheel =function(e){
	//console.log(e);
	cam.events(e);
	scroll=scroll+2;
	if(e.wheelDelta>0){
		wheelDelta=1;
	}
	if(e.wheelDelta<0){
		wheelDelta=-1;
	}
	if(cam.pos[1]<10 && cam.pos[1]>-10){
		if(wheelDelta>0){
			cam.update(e.wheelDeltaY/1000,{s:true,z:false});
		}
		if(wheelDelta<0){
			cam.update(-e.wheelDeltaY/1000,{z:true,s:false});		
		}
	}

}

document.onmousedown = function(e){
	if(mousedown){

	}else{
		mousedown=true
	}
}

document.onmouseup = function(e){
	if(mousedown){
		mousedown=false
	}
}



window.onmousemove = function(e){	
	if(mousedown){		
		cam.events(e);
	}
}

