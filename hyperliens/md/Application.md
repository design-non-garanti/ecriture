### Application

- The term app has become a popular buzzword; in January 2011, app was awarded the honor of being
  2010's "Word of the Year" by the American Dialect Society. "App" has been used as
  shorthand for "application" since at least the mid-1990s, and in product names since at least
  2006, for example then-named Google Apps. (w!)
- The Slavery of "Applicatons" : In the nineteen-sixties, IBM invented customer slavery-- because
  the products were beyond the user's control, the user had to pay not just for software, but for
  upgrades, and for the suffering of living through those upgrades.  That's like certain governments
  billing the family for the bullet used to kill their son.  In 1974, when I published Computer Lib,
  there were relative few computers, and they were big beasts in airconditioned rooms that were
  programmed oppressively.  Now, with today's computers, you can be oppressed in your own living
  room!  And millions are.  An "application" is a closed package of function.  You don't own your
  data, THEY do.  You don't control the interface, THEY do.   You have only the options they give
  you.  They can change the software, make you buy the new version, and make you endure the
  inconvenience of learning and adapting to the new version.  Which is very probably not what you
  want, but you can't change it, you have to learn to live with it.  Every "application" has its own
  way of dividing its domain into parts, tying the parts together, and accessing the parts-- the
  same things that other software does, in the same domain and other domains.  But the controls and
  options are all different, and you can't carry them across applications.  You have to learn
  special, local ways to do what you should be able to do freely.  Applications are prisons, going
  from application to application is like prisoners being bussed between prisons, and "exporting"
  and "importing" data is like mailing something to a prisoner.  In Unix, you can pretty much do
  anything.  There are no "applications".  You can run any program on any data, and if you don't
  like the results, throw them away.  Computer liberation will mean empowering users to have this
  same kind of control. (Nelson)
- Accès au contenu web à travers des applications (code fermée) à la place de langages web
- Icône devient le programme, l'icône est à la fois l'ouverture de son exécution et la fermeture de
  son code
