Nous pouvons définir l'algorithme comme étant une suite finie et non ambiguë d’opérations et/ou
d’instructions permettant de résoudre un problème donné exprimé dans un langage générique.  Le
programme comme étant l'expression d'un algorithme dans un langage donné pour une machine donnée
qui, contrairement à l'algorithme, n'est lui pas forcément fini.  Le logiciel peut être utilisé pour
identifier le(s) programme(s) et autres élements relatifs (images, icônes, base de données,
documentation, etc.) à son exécution. Dans le cas d'un logiciel qui enregistre des noms et adresses
dans une base de donnée, le programme et la base de données font partie du logiciel mais la base de
donnée n'est pas un programme.

Dans *The Art of Computer Programming* Donald Knuth compare l'algorithme à une recette de cuisine où
les instructions sont les étapes de préparation; l'input, les ingrédients; l'output, le dîner[^3].
Si un algorithme ne se résume pas à une recette de cuisine et inversement permet de mettre en avant
deux éléments. D'une part, la programmation, tout comme la cuisine, peut exprimer une
intentionnalité, d'autre part, en examiner le code source d'un programme revèle des informations sur
son propos tout comme les ingrédients et les étapes de préparation d'une recette revèle des
informations sur le plat préparé.
