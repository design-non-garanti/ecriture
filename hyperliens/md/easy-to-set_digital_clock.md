Comparant la difficulté de régler l'heure sur une montre électronique à quatre boutons à celle d'une
montre mécanique, Raskin affirme que même les tâches simples se sont embourbées dans la complexité.
Il montre alors le design d'une montre électronique présente sur un VCR qu'il qualifie de facile à
régler cependant il soutient qu'un design encore meilleur serait une horloge qui se règle elle
même en fonction de signaux horaires[^1].

[^1]: Jef Raskin, *The Humane Interface*, Addison Wesley, 2000, p. ?

![« An easy-to-set digital clock on a VCR » présenté par Raskin.](../items/easy-to-set_digital_clock/easy-to-set_digital_clock_orig.png "easy-to-set_digita_clock_orig.png")

On peut se demander si le design de la montre électronique imaginée par Raskin serait celle-ci :

![« An easy-to-set digital clock on a VCR » réglable par incrémentation et décrémentation ou par synchronisation au signal horaire.](../items/easy-to-set_digital_clock/easy-to-set_digital_clock_time.png "easy-to-set_digita_clock_time.png")

Ou celle-là :

![« An easy-to-set digital clock on a VCR » réglée par synchronisation au signal horaire.](../items/easy-to-set_digital_clock/easy-to-set_digital_clock_auto.png "easy-to-set_digita_clock_auto.png")

- Pourquoi laisser les boutons permettant le réglage par incrémentation et décrémentation si le réglage par signal horaire est probablement supérieur en précision et ne nécessite qu'un seul bouton à presser ? 
- De ce fait pourquoi même laisser un bouton permettant d'activer la synchronisation par signal horaire si cela est la seule possibilité de réglage ? 
- *Keep the Simple Simple* (Raskin)
- Une montre est faite pour être à l'heure.
- Si la première et la deuxième permettent au retardaire occasionel d'avancé sa montre de 5 minutes
pour être à l'heure, la troisième, synchronisée automatiquement par signaux horaires, confiante que
son automatisme la rend parfaite, n'ouvre pas au réglage et restreint donc les usages non prévus.
- Simplicité vers automatisation ?
- quand cette automatisation devient non-débridable, ou cache le processus le fonctionnement
- réduit l'interaction, possibilité d'intervenir, et considération des utilisateurs comme ignorants


