

What is your name ?

Please enter your address

Where did you first hear about this game ?
	advertisement
	catalogue
	store
	friend
	other

Sex?
	Male
	Female
	Other

What is your favorite color?
	blue
	green
	yellow
	red
	purple

What is your quest ?

	Lire ce mémoire
	Juger ce mémoire
	Apprendre quelque chose grâce à ce mémoire
	Découvrir quelque chose grâce à ce mémoire
	Vivre une expérience grâce à ce mémoire
	Évaluer ce mémoire
	Évaluer l'auteur de ce mémoire
	Apprendre à mieux connaître les auteurs de ce mémoire
	Finir ce mémoire
	...

What is the capital of Assyria
	
	Ninevah. No! Wadi
	Ur
	Uhhmm
	Maybe
	Camelot

Pourquoi même un bon design peut-il échouer ?

Pourquoi l'ordinateur personnel est-il si complexe ?

Un outil peut-il être conçu pour être détourné ?

Pourquoi ne peut-on pas mettre des notes marginales sur un fichier Microsoft Word, un Pdf ou un site web ?

Pourquoi avons-nous des bureaux d'ordinateur et pas des ateliers d'ordinateur ?

Pourquoi le texte à l'écran est-il généralement en noir sur fond blanc ?

Comment disposez-vous les îcones sur le bureau de votre ordinateur ? 

Quand votre ordinateur ne fonctionne pas comme vous le voulez d'où viens le problème ? 

	de vous-même
	de l'ordinateur
	du concepteur du logiciel

Êtes-vous 
	protestant 
	catholique

Avez-vous un
	Window
	Macintosh

Vous utilisez quotidiennement

	une interface graphique
	une interface en lignes de commandes

Quelle est la différence en innovant et novateur ?

Vous êtes ?

Un amateur
Un professionel
Un expert

Un utilisateur
Un individu

Un développeur

Mais encore ?

Quel est votre age ?

Un ordinateur est-il une surface ?

C'est quoi un ordinateur ?

Surfez-vous sur internet ?

Saviez-vous que le surf sur le web est mort en 1996 ?

Saviez-vous que les utilisateurs d'internet

Un bon design est 

	pratique
	élégant
	intelligent
	facile d'utilisation
	compréhensible par tous
	innovant

Un bon design
	
	répond à un besoin
	définit un besoin
	ne se contente pas de répondre à un besoin, il le définit

Un téléphone est-il un ordinateur ?

L'ordinateur est-il un téléphone ?

L'écriture est 

	une technologie
	une tâche difficile
	archaïque
	omniprésente
	un espace de lecture
	un espace d'individuation
	un outil de contrôle

L'ordinateur est-il 

	une aventure civile
	une aventure commerciale
	la première et la deuxième question signifient la même chose

Qu'est ce qu'internet ?

	un outil commercial
	un réseau social
	une démocratie
	une encyclopédie
	un divertissement

Un développeur est t-il un utilisateur ?

Un utilisateur est-il un développeur ?

Que devrait-être internet ?

	un outil commercial
	un réseau social
	une démocratie
	une encyclopédie
	un divertissement

À quoi vous sert internet ?

À quoi sert un ordinateur ?

À quoi vous sert un ordinateur ?

l'intelligence est-elle naturelle ou artificielle ?

Démocratie est t-il un synonyme de pertinence ?

Popularité est t-il un synonyme de pertinence ?

Parmi les termes suivants lequel est-il le plus correct pour désigner un contenu pertinent

	Populaire
	Démocratique
	Recommandé
	Authentique
	Unique
	Classique

Un bon design doit-il avoir raison ? 

Le design existe il pour servir ?

Cochez :

Sécurité
Facilité
Rapidité
Accessibilité
Élégance
Simplicité
(Innovation)

Choisissez la bonne position pour chaque forme dans chaque situation : 

(jeu du carré, triangle, rond)

>Every shape exists only because of the space around it… Hence there is a ‘right’ position for every shape in every situation. If we succeed in finding that position, we have done our job.[^]

[^]: — JAN TSCHICHOLD, Typographische Gestaltung (1935)

>The use of ornament. in whatever style or quality. comes from an attitude of childish na·ivety. It shows a reluctance to use "pure design, " a giving-in to a primitive instinct to decorate - which reveals. in the last resort. a fear of pure appearance. It is so easy to employ ornament to cover up bad design! The important architect Adolf La os. one of the first champions of pure form. wrote already in 1898: "The more primitive a people. the more extravag antly they use ornament and decoration. The Indian overloads everything. every boat. every rudder. every arrow. with ornament. To insist on decoration is to put yourself on the same level as an Indian. The Indian in us all must be overcome. The Indian says: This woman is beautiful because she wears golden rings in her nose and her ears. Men of a higher culture say: This woman is beautiful because she does not wear rings in her nose or her ears. To seek beauty in form itself rather than make it dependent on ornament should be the aim of all mankind."[^]

[^]: — The important architect Adolf La os. one of the first champions of pure form. wrote already in 1898

Caractérisez l'usage de l'ornement :

Enfantin
Refus de la pureté du design
Instinct primitf
La peur de la réélle beauté 
Maquillage pour un mauvais design
Bon pour les Indiens
Basse culture

Qui est le maître de la simplicité ?

John Maeda

Cherchez l'intru :

Technologique
Digital
Numérique
…

Parmis les mots suivants, choisissez un miracle :

Apple
Xerox
Google
Microsoft
iPhone
…

Pourquoi dix-huit siècles de culture de l'imprimé ont été réduit à une simulation de feuille de papier ?

>why eighteenth-century typesetting in summary the pooi dumbed down the computer to a paper simulator what park did was make a machine for secretaries and not even for high-level secretaries for typists so they wouldn't have to type a document repeatedly[^]

[^]: — Ted Nelson -- Computers for Cynics [full version] youtube retranscription 2012


Pourquoi ?
   ______
 _|______|       
|        |_____
|        |  |  |  
|________|  |  |
            |  |
 ____       |  |
|   | \     |  |
|   |__\    |  |
|       |___|  |  
|       |      |  
|_______|      |
               |
 ____          |
|   | \        |
|   |__\       | 
|       |______|  
|       |
|_______|  
 
 ____   
|   | \ 
|   |__\ 
|       |______
|       |   |  |
|_______|   |  |
            |  |
   ______   |  |
 _|______|  |  |
|        |__|  |
|        |     |
|________|     |
               |
   ______      |
 _|______|     |
|        |_____|
|        |
|________|

