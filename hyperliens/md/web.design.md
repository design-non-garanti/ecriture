 
#Nous recherchons un 
web designer–graphiste¹

Mission du poste :

En étroite collaboration avec le Directeur de Création et le DA / Lead Ux Designer, vous participerez : au recensement et à la spécification des besoins clients et utilisateurs… À la définition de personas et des scénarios utilisateurs…

La facilité d’utilisation des produits et l’expérience utilisateur devront être au coeur de tes réalisations.

Tu pourras innover, inventer, créer, imaginer, te tromper, recommencer, améliorer, pour créer ce petit plus qui fait 
la différence.

Vous serez associé(e) à d’autres designers, juniors et seniors pour imaginer les interfaces de demain.

Au sein de l’équipe, vous aurez l’occasion de mettre en pratique ou d’approfondir vos connaissances en design thinking, ergonomie cognitive, creative problem solving, et business model génération.

Vous serez amené à développer vos compétences sur des projets innovants orientés grands comptes. Vous travaillerez dans un environnement pluridisciplinaire : mobiles natives iOS, Android, hybrides, Web et Backend, et en étroite collaboration avec des ergonomes, des UX designers et des graphistes.

15
¹ Le texte qui suit est constitué d'extraits d'offres d'emplois dans la région de Grenoble obtenus par la recherche associé des termes «web» «designer» et «graphiste» sur le site www.indeed.fr recherche effectuée le 03/09/2017


Votre rôle sera de faire en sorte d’améliorer sans cesse les performances du site, tout en travaillant avec différents interlocuteurs, notamment un data scientist pour travailler sur 
les données, et les exploiter au maximum.

Vous intervenez sur l’animation d’un atelier de travail pour le développement d’une application de gestion de Workflow. 
Vous assurez la rédaction de spécifications générales, 
la planification et le suivi du plan de charge (pilotage en mode Agile).

Vous aimez la technique, vous souhaitez travailler dans une équipe Agile et innovante ?

Votre immersion dans nos projets vous permettra d’acquérir une expérience significative aux environnements techniques et fonctionnels de notre client et de son métier.

En agence, vous intégrerez une équipe fonctionnant en méthode agile SCRUM et interviendrez dans les différentes phases de l’élaboration des applications pour les clients.

Vous êtes capable de conseiller et d’accompagner avec pertinence nos clients sur la conception visuelle digitale et UX design de leurs projets web, newsletter, bannière web, etc…

Concevoir des webdesign adaptés.

Donner la priorité à des délivrables efficaces, innovants 
et «user–friendly».

Profil recherché :
Un certain goût pour l’UX design est souhaité.

Volonté de travailler en utilisant les méthodes agiles.

A minima 2 ans d’expérience dans le domaine de la conception d’expérience utilisateur. Bonne maîtrise des méthodologies Ux.

Connaissance des méthodes agiles. Connaissance (ou envie de progresser) des pratiques de design thinking.

Tu es créatif afin d’apporter des plus values techniques 
et fonctionnelles.

Tu es orienté design afin d’apporter des solutions pratiques 
et esthétiques.

Tu as idéalement une première expérience en UX/UI Design.

Tu travailleras dans un environnement innovant avec une ambiance dynamique et créative. Tu participeras à toutes les phases du projet au travers de méthodologies agiles et créatives.

Votre domaine d’expertise, c’est le design d’interaction, UX et UI ; mais vous êtes aussi ouvert à d’autres disciplines comme le design produit ou la programmation (HMTL/CSS/Arduino/JS)•Polyvalent, vous savez aborder votre projet avec un regard neuf afin d’imaginer des solutions concrètes, centrées sur les usages.

L’Agilité et ses Valeurs font écho en vous ?

Tu pourras acquérir de l’expérience dans des domaines innovants dans une ambiance dynamique et créative en immersion dans l’environnement et les méthodologies créatives et agiles.

Vous vous projetez dans notre environnement de travail dynamique, réactif et innovant ? Venez rejoindre une équipe travaillant dans un cadre Agile Scrum qui a à cœur de créer un code propre, réfléchi et pérenne !

Maîtrise du design et de l’ergonomie.

Vous êtes à l’aise avec les méthodes agiles et des notions de pratiques de design thinking. Votre réel intérêt pour le monde du digital n’est plus à prouver, vous êtes rigoureux, à l’écoute, 
ouvert d’esprit.
 