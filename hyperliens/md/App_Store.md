  
- Réguler la distribution, Contrôler le developpement, Dissimuler la programmabilité
- « We define everything that is on the phone », John Markoff, « *Steve Jobs Walks the Tight Rope
  Again* », 12/01/2007, consultable à l'adresse :
  <http://www.nytimes.com/2007/01/12/technology/12apple.html>
- « You don’t want your phone to be like a PC. The last thing you want is to have loaded three apps
  on your phone and then you go to make a call and it doesn’t work anymore. These are more like
  iPods than they are like computers. », « These are devices that need to work, and you can’t do
  that if you load any software on them », Ibid.
- « That doesn’t mean there’s not going to be software to buy that you can load on them coming from
  us. It doesn’t mean we have to write it all, but it means it has to be more of a controlled
  environment. », Ibid.
- Discours utilisateur comme ignorants, victime, tellement que si on lui laisse la main sur
  son iPhone il peut le « cassé » (Breaking News: On peut casser des choses avec du code)
- Discours basé sur la peur
- L'App Store s'impose comme « The only way » (Jobs) de distribuer des applications 
- Soumis aux SDK(software development kit), 70/30,  guidelines d'Apple, à l'approbation, à la licence (pas compatible avec GNU GPL)
-  L'App Store se situe comme intermédiaire entre les utilisateurs. Ce modèle de distribution est similaire à celui des centres d'appels téléphoniques. Lorsqu'un utilisateur du service téléphonique souhaité communiquer avec un autre la communication été routée par et passé par le centre d'appel. De la même manière lorsqu'un utilisateur souhaite fournir une application à un autre cette transaction est contrôlé par Apple et passe par l'Apple Store.
- Jobs instaure une situation de contrôle
  - « The only way to distribute software »
  - éviter que les gens redéfinisse usages
  - kill switch supprimer nimporte quelle application sur nimporte quel iphone > contrôle /
    surveillance total
  - brick mise a jour qui bloque les tels modifiés > punition des usagers / des mauvais usages
  - Apple II VisiCalc / iPhone App Store
  - Maintenant app store pour MacOs (échange de la programmabilité contre la simplicité)

- *L'argument est le suivant : les applications extérieures cassent l'iphone car elles ne sont «pas
  faites pour l'iphone (I user mais aussi I apple)» un déplacement de «elles ne sont pas faites par
  nous» *Apple rends service aux usagers en les protégeant de leur propre bétise, il les sauvent
  d'eux-même, (l'utilisateur victime de lui-même autre manière de victimiser que celle de Don Norman
  qui considère l'utilisateur victime de la technologie)
- C'est pourquoi, comme le précise Jobs, tout est définit sur l'iPhone ("We define everything that
  is on the phone").  L'utilisateur est incapable de «bien designer» pour Apple car il n'est pas
  Apple, c'est une loi. La bonne manière ne dépendant nullement de capacités techniques ou
  intellectuelles ou d'une quelconque mesure de pertinence mais simplement d'une position, être
  Apple ou ne pas l'être.  iApple.
- In 2007, Apple Computer launched the iPhone, the company's first ever smartphone. When the device
  launched, the device did not provide any support for third-party software: Apple's CEO Steve Jobs
  believed that web apps served over the internet could provide adequate functionality required for
  most users. Soon after its release, however, developers had managed to "jailbreak" the iPhone and
  begin coding third-party apps for the device, distributed through package managers such as
  Installer.app (which itself was based on APT) and Cydia (w!)
- Impossible de forcer le bon usage d'un objet (ordinateur comme cale table ?)
- October 17, 2007 that the iPhone software development kit for third-parties would be released on March 6, 2008.
 