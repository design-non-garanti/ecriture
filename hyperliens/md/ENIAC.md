
Si l'on prend l'exemple de l'ENIAC (1946) on découvre un autre rapport à la programmation. Premier
ordinateur électronique dit Turing-complet, il pouvait être reprogrammé pour résoudre, en principe,
tous les problèmes calculatoires. Contrairement aux ordinateurs plus récents, ce n'était pas un
ordinateur à programme enregistré (*stored program*). Sa reprogrammation nécessité une intervention
sur le matériel (*hardware*) : manipuler des commutateurs et recâbler un réseau de fils électriques
car le programme n'était pas stocké électroniquement dans la mémoire. La reprogrammation était certe
une tâche longue et complexe mais dans le cas de l'ENIAC empêcher la reprogrammation nécessite de
bloquer l'accès, physiquement, à l'ordinateur. 
