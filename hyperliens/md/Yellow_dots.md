Les recherches de l'Electronic Frontier Foundation (EFF) montre que la plupart des imprimantes laser
couleur ajoutent intentionnellement des informations presque invisibles sur toutes les pages
qu'elles impriment qui permettent de tracer les documents imprimés.  Les imprimantes Xerox de la
série DocuColor par exemple, impriment une grille de 15 par 8 minuscules points jaunes qui permet
d'encoder des informations telle que la date et l'heure d'impression ainsi que le numéro de série de
l'imprimante[^1], ces informations sont presque impossible à percevoir sans l'aide d'un microscope
ou d'une lumière bleue.  

L'EFF effectue des tests et répertorie sur son site[^2] les imprimantes affichant ce type de
marquage sur les documents imprimés et propose de télécharger, d'imprimer et de renvoyer des pages
de tests[^3] afin d'enrichir cette liste et de préciser cette enquête. Si la forme de ces tatouages
varient en fonctions des imprimantes et des fabriquants et que la plupart reste encore à décoder
l'EFF propose un programme[^4] permettant de traduire les informations pour les imprimantes Xerox de
la série DocuColor.

![Marquage sur Xerox DocuColor 12, agrandit 60 fois, photographié par un microscope numérique QX5 de Digital Blue.](../items/yellow.dots/xerox_doc12_test01.jpg "xerox_doc12_test01.jpg")

![Marquage sur Xerox DocuColor 12, agrandit 10 fois, de plus gros points ont été ajoutés pour permettre une meilleur lisibilité ainsi que le texte décrivant leurs significations.](../items/yellow.dots/eff_yellow-dots_4.png "eff_yellow-dots_4.png")

![Microscope numérique QX5 de Digital Blue.](../items/yellow.dots/digital.blue_qx5_digital.microscope.jpg "digital.blue_qx5_digital.microscope.jpg")


Ce dispositif décrit comme une puce situé à l'intérieur de l'imprimante à proximité du laser qui
intègre les points juste avant l'impression aurait été mis en place par les fabriquants
d'imprimantes à la demande de gouvernement afin de lutter contre les contrefaçons (billets de
banque, documents d'identité, etc) mais permet finalement de tracer n'importe quel document.

La restriction d'accès au code source qui, en 1980, contraignait la modification du programme de la
Xerox 9700 se traduit aujourd'hui en une opacité qui permet d'implémenter secrètement des
fonctionnalités à l'insu de l'utilisateur et ceci même en dehors de l'espace numérique.



[^1]: EFF, « DocuColor Tracking Dot Decoding Guide »,
<https://w2.eff.org/Privacy/printers/docucolor/>

[^2]: EFF, « List of Printers Which Do or Do Not Display Tracking Dots »,
<https://www.eff.org/pages/list-printers-which-do-or-do-not-display-tracking-dots>

[^3]: EFF, « Investigating Machine Identification Code Technology in Color Laser Printers »,
<https://w2.eff.org/Privacy/printers/wp.php#testsheets>

[^4]: EFF, « DocuColor Tracking Dot Decoding Guide »,
<https://w2.eff.org/Privacy/printers/docucolor/>
