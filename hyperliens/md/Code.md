- Le code est un langage qui est executable.
- Les mots font ce qu'ils disent. Le code est un langage qui fait ce qu'il dit.
- 2 états un état legible et un état executable
- code in culture

Dans un article publié en 2000 le juriste Lawrence Lessig présente le code informatique comme
architecture du cyberespace\footnote{Le terme \enquote{cyberspace} est employé pour désigner
l'espace de communication activé par le réseau Internet plutôt que son ensemble matériel, terme
popularisé notamment par John Perry Barlow.}. Le code \enquote{définit la manière dont nous vivons
le cyberespace\autocite{lessig-law}}, il influe sur des enjeux majeurs comme la vie privée, la
censure, l'accès à l'informations, surveillance, liberté d'expression au sein de celui-ci.
\enquote{Le code régule. Il implémente – ou non – un certain nombre de valeurs.  Il garantit
certaines libertés, ou les empêche}. Lessig évoque les protocoles TCP/IP qui permettent le transfert
de données sur Internet sans que les réseaux aient connaissance du contenu des données, et sans
qu’ils sachent qui est réellement l’expéditeur de données. Ces spécifités restreignent la
possibilité de réguler les activités sur Internet. 

D'une manière générale les caractéristiques aux
origines d'Internet comme les protocoles TCP/IP, la commutation de paquets, l'architecture
distribuée, traduisent une volonté d'ouverture et de décentralisation et empêchent une institution
de contrôler la manière dont circule l’information.  Cependant Lessig précise que ce code, cette
architecture n'est pas fixe et définitive, qu'elle peut et est en train de changer, que d'autres
couches de code peuvent être superposées\footnote{Nous pouvons par exemple citer le \textit{Deep
Packet Inspection} (DPI) qui permet d'inspecter le contenu des données qui circule sur
Internet.}. Pour lui il est important d'\enquote{examiner l’architecture du cyberespace}, il
s'interroge alors sur notre place dans le choix de celle-ci au risque de laisser ces questions à une
minorité d'experts.
