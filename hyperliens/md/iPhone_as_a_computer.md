Apple, par exemple, a adopté un discours qui ne présente pas l'iPhone comme un ordinateur. 
Annoncé au public par Steve Jobs lors d'une des célèbres keynotes d'Apple le 9 janvier 2007 
pendant le congrès Macworld au Moscone Center de San Francisco, cette présentation introduit
l'iPhone (qui sera commercialisé environ 6 mois plus tard) et ses fonctions clés.

Jobs revient sur le Macintosh (1984) et l'iPod (2001) présentés comme deux produits révolutionnaires
d'Apple ayant respectivement changés l'industrie informatique et l'industrie de la musique afin de
déclarer que 3 produits du même acabit seront annoncés aujourd'hui. En effet, Jobs présente l'iPhone
comme la combinaison d'un téléphone, d'un iPod et d'un navigateur web[^1] -- une combinaison de
service servant bien.  

Durant l'ensemble de la présentation l'iPhone n'est pas adressé en tant qu'ordinateur, au contraire,
il est question de pouvoir « faire comme sur un ordinateur[^2] ». Cette opposition se confirme à la
fin de la présentation lorsque Jobs usant d'une question rhétorique nous rapelle que parmit le Mac,
l'iPod, l'AppleTV et l'iPhone, le Mac est le seul que nous pouvons vraiment considérer comme un
ordinateur.  D'ailleurs afin de refléter au mieux cet aspect Jobs annonce que le mot « computer »
sera retiré du nom de la marque faisant passer « *Apple Computer Inc* » à « *Apple Inc* »[^3]. 

Ces éléments de discours participe à faire circuler une certaine image de l'iPhone, comme le précise
Jobs celui-ci ne doit pas être pensé comme un ordinateur[^4]. 

 
Pourtant lors du lancement Jobs revandique que l'iPhone fonctionne avec OSX, le système
d'exploitation développé et commercialisé par Apple depuis 1998 sur lequel les ordinateurs d'Apple
fonctionnent, ces quelques exemples ne sont donc pas impossible à mettre en place mais bien des choix
de design. 
 

[^1]: « So, three things: a widescreen iPod with touch controls; a revolutionary mobile phone; and a
breakthrough Internet communications device.  An iPod, a phone, and an Internet communicator. An
iPod, a phone … are you getting it?  These are not three separate devices, this is one device, and
we are calling it iPhone. » *iPhone Introduction*, 09/01/2007, 00:24:02, consultable à l'adresse :
<https://www.youtube.com/watch?v=-3gw1XddJuc> 

[^2]: « I can look at my e-mail with a split view just like I do on my computer [...] So it’s real
e-mail, just like you’re used to on your computer, right here on your phone », Ibid., 1:00:00

[^3]: « So, today, we’ve added to the Mac and the iPod. We’ve added Apple TV and now iPhone.  And
you know, the Mac is the only one that you think really of as a computer. Right?  And so we’ve
thought about this and we thought, you know, maybe our name should reflect this a little bit more
than it does.  So we’re announcing today we’re dropping the computer from our name, and from this
day forward, we’re gonna be known as Apple Incorporated, to reflect the product mix that we have
today. », Ibid., 1:37:42

[^4]: « I don’t want people to think of this as a computer », John Markoff, *Steve Jobs Walks the
Tight Rope Again*, 12/01/2007, consultable à l'adresse :
<http://www.nytimes.com/2007/01/12/technology/12apple.html>
