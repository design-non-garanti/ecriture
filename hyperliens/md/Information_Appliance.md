Dans la préface de « The Invisible Computer » Don Norman exprime clairement un des objectifs de son
livre : accélérer le jour où l'ordinateur disparaisse hors de la vue et que la technologie qui le
remplace soit facile à utiliser.  Afin de dompter la technologie Don Norman propose une méthode : les
« informations appliances »  l'ordinateur invisible sera le résultat.

Le terme est attribué à Jef Raskin qui l'aurait proposé en 1978 (basiquement le gars qui a pondu le
Macintosh).


L'ordinateur devrait être silencieux, invisible, unobtrusive, mais il est trop visible, trop
demandant. Ses complexités et frustrations sont largement due au fait que l'on essaye de fourrer
trop de fonctions dans une seule boîte posé sur le bureau.


- simplicité, 
- extension naturelle de la personne
- specialisation de la fonction


Dans   l’ouvrage   « The   Invisible   Computer »,   Don   Norman   définit   les information a
ppliances comme  des  produits  «  spécialisés  dans  l’information » (Norman,  1999a,  p.53) ,
permettant  aux  utilisateurs  d’accomplir  une  tâche  précise.  Il  propose  ainsi  de  s’écarter
du modèle   générique   de   l’ordinateur,      jugé   trop complexe   pour   un   usage
quotidien,   afin d’envisager des produits plus simples, souples et agréables à utiliser (p.67).
Pour Norman, c’est en limitant le nombre de fonctions des produits que le concepteur pourra produire
la structure et les  interactions  les plus  adaptées.  Il  invite  les  designers  à  développer
des  interactions  directes avec le produit, permettant à l’utilisateur de se concentrer sur la
tâche plutôt que sur la manière de l’opére

- Information appliance comme solution
- « Recall the fundamental difference between a PC and an information appliance: the PC can run code
from anywhere, written by anyone, while the information appliance remains tethered to its maker’s
desires, oﬀering a more consistent and focused user experience at the expense of ﬂexibility and
innovation. » (Zittrain)
