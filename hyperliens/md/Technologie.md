
Dans Not Art&Tech[^1] Olia Lialina expose la tendance à remplacer le mot « ordinateur » par le mot «
technologie ». 

Cette substitution qu'elle qualifie au départ de synedoque généralisante[^2],

entraînerait une dissolution de l'ordinateur dans toutes les autres technologies, le rendant
invisible et ferait circuler l'image de l'ordinateur comme étant juste une technologie parmit les
autres.

Comme le précise Lialina cette tendance est dans l'intérêt de l'industrie, car elle rend les
utilisateurs ignorant de l'ordinateur comme système qui est programmé, qui peut être reprogrammé,
qui peut potentiellement être programmé ou reprogrammé par leurs utilisateurs.

    
    
Pour Ted Nelson l'emploi du terme « technologie » est presque toujours un geste politique :
comme « maturité », « réalité » et « progrès », « technologie » est employé pour se référer à
quelque chose à laquelle quelqu'un veut que vous vous soumettiez, la « technologie » doit être
laissé à « ceux qui la comprennent », aux « technologistes »[^3].


Don Norman précise nous utilisons le mot technologie pour nous référer à ce qui est nouveau.
L'ordinateur est appeller « technologie ». Internet est appeller « technologie ».  Au contraire
le crayon ou le papier sont si banal que nous les tenons pour acquis.


[^1]: Olia Lialina, *Not Art&Tech*, 2015/11, consultale à l'adresse :
<http://contemporary-home-computing.org/art-and-tech/not/>

[^2]: Figure de style qui consiste à nommer le tout pour signifier la partie (son vélo a crevé
pour un pneu de son vélo), stylistiquement, la synecdoque généralisante tend vers l'abstraction.

[^3]: Ted Nelson, *Ted Nelson's Computer Paradigm, Expressed as One-Liners*, consultable à l'adresse
: <http://xanadu.com.au/ted/TN/WRITINGS/TCOMPARADIGM/tedCompOneLiners.html>

