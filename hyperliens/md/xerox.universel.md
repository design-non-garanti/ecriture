 
## Xerox : l'universel 

- Origine du terme Xerox (faire comme kodak, emprunt au grec, langage universel, bible et grec)
- Publicités sur l'accessibilité puis le frère Dominique (rapport à l'écrit biblique)
- Histoire de l'impression, diffusion des textes / lois à grande échelle / masse
- Réforme protestante arrêt de la messe en latin (universalité) / invention de l'imprimerie

## Origine du GUI chez Xerox

- Gui c'est le papier à l'écran
- Xerox Alto / Star
- WYSIWYG WYSIWYP
- modèle de l'imprimé
- détermination des interfaces
- origine pensée UX
- Ivan Sutherland, SketchPad
- Direct Interaction

 Le mode graphique de la matrice 
Le mode graphique d'une interface représente l'idéal de transparence explicite, langage universel datant de la renaissance et l'on retrouve cette pensée dans l'histoire de Xerox et de sa publicité.

>Inversement, l'IHM réinvente le paradigme du langage pictural universel des signes, d'abord envisagé dans les utopies éducatives de la Renaissance, de la Cité du Soleil de Tommaso Campanella à Jan Amos Comenius, en passant par le livre scolaire illustré "Orbis Pictus". Leurs objectifs de conception étaient similaires:"utilisabilité", fonctionnement explicite à travers différentes langues et cultures humaines, si nécessaire au détriment de la complexité ou de l'efficacité. 

    florian cramer $(echo echo) echo $(echo)_ Command Line Poetics — LGRU Reader

 
Utopies éducatives
Villes nouvelles, villes utopies (années 60-70)
XVIIIe Salines Arc et Senans Claude-Nicolas Ledoux  

  points communs dates, grecs, universalité, renaissance réaction à l'entropie de la seconde ?  

 
Entre 1935 et 1938 Chester Carlson s’intéresse à la photoconductivité et particulièrement à un article de l'ingénieur Pál Selényi (Tungsram entreprise hongroise d'ampoules et électronique), après plusieurs expérimentations ratées, quelques conseils de son épouse, l'utilisation d'un local appartenant à sa belle-mère et l'aide d'un physicien allemand réfugié, il fini par mettre au point un procédé d'impression qu'il nomme électrophotographie. D'abord, il faut écrire à l'encre sur une lame de verre, puis il faut frotter une plaque métallique recouverte de soufre avec un chiffon pour lui donner une charge électrique. Il faut placer la glissière contre la plaque, placer l'ensemble sous une lampe puissante pendant quelques secondes, enlever la glissière et saupoudrer du pigment sur la plaque. L'inscription apparaît. Cette technique est  celle des imprimantes laser, LED et employée  dans la plus part des photocopieuses et le pigment saupoudré sur la plaque est appelée toner. Carlson tente ensuite d'obtenir sans succès des financements de la part d'IBM, Kodak, General Electric et de RCA pour développer son invention.
C'est par le biais d'une association à but non-lucrative qui finit par obtenir un accord avec une entreprise spécialisée dans les services et produits photographiques nommée Haloid en 1947. En 1948 l'électrophotographie est renommée Xerographie du grecque *xeros* pour sec et *graphis* du latin pour écriture, terme plus *intuitif* proche d'une description du procédé de Carlson et le terme Xerox deviens une marque déposée. En 1956 l'ensemble des droits sur les brevets xérographiques sont conférés à Haloid et en 1958 Haloid change officiellement de nom pour Haloid Xerox puis Xerox en 1961 – inspiré par le fait que le nom Kodak est construit autour de la même première et dernière lettre.

>Kodak est un Mot créé arbitrairement en 1888 pour ses possibilités internationales d’emprunt par George Eastman (1854-1932) qui le déposa comme nom pour son appareil photographique portatif.
https://fr.wiktionary.org/wiki/kodak#en 

Dans le magazine fortune en 1961 Xerox insiste sur le fait qu'il n'y a rien d'ancien et de grecque dans l'esprit de l'entreprise soulignant plutôt son engagement à répondre aux exigences de la diffusion de masse de l'information.

    Eva Hemmungs, NO TRESPASSING: AUTHORSHIP, INTELLECTUAL PROPERTY RIGHTS, AND THE BOUNDARIES OF GLOBALIZATION, Chapter Three THE DEATH OF THE AUTHOR AND THE KILLING OF BOOKS: ASSAULT BY MACHINE, pp 57-69

Dire qu'il n'y a rien de grec et d'ancien dans Xerox est faux car il y a tout de l'universalité occidentale.

Emmanuel Lévinas écrit: 
>«Qu'est-ce que l'Europe? C'est la Bible et les Grecs. [...]  Il faut, par l'amour de l'unique, renoncer à l'unique. Il faut que l'humanité de l'Humain se replace dans l'horizon de l'Universel. Ο messages bienvenus de la Grèce! S'instruire chez les Grecs et apprendre leur verbe et leur sagesse»2. 
    À l'heure des nations, Paris, éditions de Minuit, 1988, p. 155-156. dans Vieillard-Baron Jean-Louis. Hegel et la Grèce. In: Le Romantisme et la Grèce. Actes du 4ème colloque de la Villa Kérylos à Beaulieu-sur-Mer du 30 septembre au 3 octobre 1993. Paris : Académie des Inscriptions et Belles-Lettres, 1994. pp. 55-61.

 

Xerox c'est la bible et les grecs, confondus dans l'universalité.

  voir frère Dominique, origines du nom.  

Xerox c'est l'universalité occidentale  marchande ? 

En ce sens, la Grèce est bien l'universalité de l'Occident. 

Car Xerox est marqué par la reproduction de l'écrit et sa diffusion, l'accessibilité et les références bibliques.

La publicité de xerox stigmatise cette pensée universelle occidentale
 
C'est au Xerox Park que 
Xerox est une entreprise qui vend des imprimantes avant tout et c'est dans cette 
 
Conçu au Xerox PARC en 1973, Xerox Alto est l'un des premiers ordinateur personnels PC, c'est également le premier ordinateur à comporter une interface dite graphique et un environnement de bureau.

 interface textuelle (TUI)
 
### L'user experience c'est l'universalité occidentale, c'est à dire la bible et les grecs ? 
 

 