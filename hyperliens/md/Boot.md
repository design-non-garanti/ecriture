Dans *The Invisible Computer* Don Norman revient sur le terme *booting*, procédure de démarrage d’un
ordinateur, qui comporte notamment le chargement du programme initial.

Le terme *booting* serait dérivé de *bootstrap*, une petite boucle sur les bottes
qui aide son porteur à les enfiler. 

À la fin de son explication à la fois technique et etymologique Don Norman demande à lui-même
pourquoi il présente cette notion et soutient que l'on ne devrait ni avoir besoin de savoir ni de
s'en soucier. Ce simple passage résume, à mon avis, très bien la pensée de Don Norman.

