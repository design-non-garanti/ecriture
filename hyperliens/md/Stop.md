Un intimidant message apparaît en ouvrant la console du navigateur sur le site de Facebook.
Celui-ci affirme que la console est une fonctionnalité « conçue pour les développeurs » ainsi coller
quelque chose dans celle ici afin d'activer une fonctionnalité serait « une escroquerie ».

Informer que les manipulations effectuées dans la console peuvent avoir des conséquences quant à la
sécurité des comptes est probablement légitime de la part de Facebook, certains utilisateurs sont
effectivement victimes d'abus en collant des scripts dans cet espace, mais le ton employé ici par
Facebook renvoit plutôt l'impression que vous n'avez rien à faire ici.Le message pretextant la
sécurité de l'utilisateur, dissuade d'explorer les dessous du web, le code informatique. 

![Screenshot de la page <https://fr-fr.facebook.com/> présentant le message de prévention dans la
console.](../items/console.stop/chromium.fr.png "Screenshot de la page https://fr-fr.facebook.com/
présentant le message de prévention de Facebook dans la console.")

Pourtant puisque le code source des pages web est desservit sur les ordinateurs des utilisateurs les
outils disponibles sur les navigateurs tels que la console ou l'inspecteur d'élément permettent
simplement d'étudier, de copier ou de manipuler ce code et d'ouvrir à des [manipulations
initialement non prévues](how.to.why.leave.facebook.html).
