>Is the consumer well served?

Norman (1999), p. 27





Simondon pointe du doigt que la fonction de régleur est distincte de celle d'utilisateur de la
machine, il est même interdit aux ouvrier de régler eux-mêmes leur propre machine.

>Or, l'activité de réglage est celle qui prolonge le plus naturellement la fonction d'invention et de
>construction : le réglage est une invention perpétuée, quoique limitée. La machine, en effet, n'est
>pas jetée une fois pour toutes dans l'existence à partir de sa construction, sans nécessité de
>retouches, de réparations, de réglages.

La notion de réglage est intimmement lié à celle de possession



L'instrument de musique n'est précisément pas un instrument.
Les chaînes Hi-Fi sont des appareils qui règlent. (potentiomètre, amplificateur,
Egaliseur
 
la fonctionnalité et le service (la servilité)


Pour Pierre-Damien Huygue la demande du design s'inscrit dans un contexte de surcroît de puissance
industrielle ce qui tend à produire des objets qui dépassent les capacités des utilisateurs. Selon
lui, nous \enquote{imaginons} les objets que nous utilisons, nous ne les \enquote{réalisons} pas.

\enquote{là peut se trouver une tâche pour le design : assiter, aider, pousser par l'exemple à la production
d'objets clairs\autocite[31]{huygue-design-poussees}}
Les objets ne sont pas \enquote{clairs} avec nous, c'est-à-dire qu'ils ne sont pas francs, car la
franchise implique un rapport à l'autre.



des objets qui dépassent les capacités des utilisateurs
Cette situation donnerait à l'image une place essentielle
rompre avec ce modèle et assumer les conditions d'une pratique moins débrodée


p. 7
Pierre-Damien Huygue précise d'ailleurs que \enquote{les critiques d'allure philosophique formulée à
l'encontre de la technique s'adressent plus probablement à son
économie.}\autocite[7]{huygue-design-seminaire}


p. 35
La responsabilité des designers est de porter au public ce qui est en jeu dans les objets afin qu'il
soit possible de jouer avec ces objets réellement, et non imaginairement. \enquote{Jouer}, cela veut
dire savoir ce qu'il en est des règles de jouabilité, ce qui présuppose à son tour qu'il y ait du
jouable. Ce serait justement une vertu du design que de rendre jouables les situations objectives.
Pareille perspective suppose un travail en direction de la puissance de réglage des objets (dans mon
language : leur devenir \enquote{appareils}.

 