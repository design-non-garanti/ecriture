Communément lorsque nous parlons d'un objet transparent celui-ci est entendu comme étant un objet au
travers duquel nous pouvons voir, le préfixe *trans* signifiant « à travers » et *parens*, participe
présent de parere, « apparaître », un objet transparent laisse apparaître au travers ; au sens
figuré ce qui est transparent ne cache rien, ne dissimule rien, est franc et ouvert, nous informe
sur son fonctionnement[^1]. 

Dans le champs de l'interaction homme-machine le terme transparent porte à confusion.  Le terme est
largement utilisé comme synonyme d'invisible ou de caché. On parle d'une interface ou d'une
interaction transparente lorsqu'elle se laisse oublier, qu'elle n'apparaît pas, qu'elle ne demande
pas d'attention ou d'effort à l'utilisateur. 

Si cette idée peut sembler contradictoire avec les définitions précédentes, l'appréciation du terme
dans le champ de l'optique est éclairante. En optique un matériau transparent a pour propriété de ne
pas absorber la lumière[^2], de la laisser passer librement sans friction, la transparence désigne
donc la capacité d'un matériau à ne pas modifier l'état d'une onde de lumière en interaction.

Dans le cas de l'interaction homme-machine le terme fait référence à l'invisibilité globale de
l'objet, et non pas à la visibilité des éléments qui pourrait apparaître au travers. L'utilisation
du terme transparent en remplacement du mot invisible est aussi stratégique en terme de
communication : alors que le terme invisible connote généralement quelque chose que l'utilisateur ne
peut voir ou contrôler, transparent connote un objet qui ne cache rien[^3]. L'utilisation du terme
transparent pour impliquer l'invisible est subtilement trompeuse.

[^1]: Wiktionnaire, « Transparent », consultable à l'adresse : <https://fr.wiktionary.org/wiki/transparent>

[^2]: Wikipédia, « Tranparence (physique) », consultable à l'adresse : <https://fr.wikipedia.org/wiki/Transparence_(physique)>

[^3]: Anupam Das, « A Study of Transparency and Adaptability of Heterogeneous Computer Networks with TCP/IP and IPV6 protocols », December 2012, 

