Dans un article de 2007 dont le titre, *Where did the computer go?*[^1], reprend l'accroche de
l'iMac G5 -- le premier iMac dont « l'écran est l'ordinateur » car les designers d'Apple ont «
dissimulé le tout » dans un écran de 5cm d'épaisseur[^2] --  Dragan Espenchied soutient que
l'ordinateur (home computer) est physiquement en train de disparaître. Pour Espenchied le design des
interfaces de l'ordinateur, qu'elles soient matérielles ou logicielles, ne sont pas seulement
dictées par des nécessités techniques ou des décisions d'ingénierie mais aussi par la mode, les
images du futur, les politiques des entreprises ou le divertissement. Ces interfaces ne transportent
pas seulement des capacités de calcul à l'utilisateur mais aussi une certaine image de l'ordinateur.
Espenchied montre que déjà les publicités des années 80 présente le Commodore 64 de façon claire et
ordonnée en faisant disparaître les câbles nécessaire à son fonctionnement; le Lisa
d'Apple met à distance de l'utilisateur les parties de l'ordinateur où les données sont
traités; plus généralement les composants bruyants (ventilateurs, disques durs), rapellant les
parties mécaniques de l'ordinateur et témoignant d'une activité, éloignent d'un idéal de l'ordinateur
ne fonctionnant qu'avec des 1 et des 0 et ont donc tendance à être évités.


[^1]: Dragan Espenchied, « *Where did the computer go?* », 2007/02/22, consultable à l'adresse : <http://contemporary-home-computing.org/where-did-the-computer-go/>

[^2]: *Où est passé l'ordinateur ? Voici le tout nouveau iMac G5*, consultable à l'adresse : <https://web.archive.org/web/20040916040936/http://www.apple.com:80/fr/imac/>
