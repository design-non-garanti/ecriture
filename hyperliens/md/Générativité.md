  
- L'ordinateur est, tout comme Internet, innachevées, ils reposent sur leurs utilisateurs pour
  déterminer ce qu'il peut en être fait (Zittrain) 
- Caractère ouvert /  génératif typique
- « The Hollerith model is one of powerful, general-purpose machines maintained continuously and
  exclusively by a vendor. The appliance model is one of pre- dictable and easy-to-use specialized
  machines that require little or no mainte- nance. Both have virtues. The Hollerith machine is a
  powerful workhorse and can be adapted by the vendor to fulﬁll a range of purposes. The appliance
  is easy to master and it can leverage the task for which it was designed, but not much else.
  Neither the Hollerith machine nor the appliance can be easily repro- grammed by their users or by
  third parties, and, as later chapters will explain, “generativity” was thus not one of their
  features. » (Zittrain)
 