Étant donné que la modification ou la décompilation d'un programme compilé est problématique, la
distribution d'un programme uniquement sous cette forme en permet son utilisation tout en entravant
son étude et sa modification. L'informaticien Richard Stallman raconte que lorsqu'en 1980 le
laboratore d'Intelligence Artificielle du MIT (Massachusetts Institute of Technology) reçoit une
nouvelle imprimante, celle-ci a, tout comme l'imprimante précédente, tendance à se bloquer. Mais si
les informaticiens du MIT AI lab avaient pu modifier le programme de l'imprimante précédente afin de
contourner le problème ils étaient cette fois, malgré qu'ils soient techniquement apte,
volontairement bloqués par Xerox qui ne voulait pas fournir le code source[^1].  En ce sens la forme
sous laquelle un logiciel est distribué en régule son développement.

[1]: Richard Stallman, Conférence donnée à Paris 8 (transcription), 10 novembre 1998, consultable à l'adresse : <http://www.linux-france.org/article/these/conf/stallman_199811.html>
