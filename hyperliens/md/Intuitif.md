  
 "Oh, SURE the Macintosh interface is Intuitive!
I've always thought deep in my heart that command-z should undo things."
-- Margy Levine

>The term "intuitive" for interfaces is a complete misnomer.  I can think of two meanings in
>software for which the term "intuitive" is presently used:
>1.  Almost nobody, looking at a computer system for the first time, has the slightest idea what
>it will do or how it should work.  What people call an "intuitive interface" is generally one
>which becomes obvious as soon as it is demonstrated.  But before the demo there was no intuition
>of what it would be like.  Therefore the real first sense of "intuitive" is retroactively
>obvious.
>2.  Well-designed interactive software gradually unfolds itself, as in the game of Pac-Man,
>which has many features you don't know about at first.  The best term I've heard for this is
>self-revealing (term coined by Klavs Landberg).

 