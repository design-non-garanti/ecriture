# Pourquoi le texte c'est moche ?


<br>//////////////// début des notes 



 
- Questionner les termes, les distinctions : Interface textuelle / Interface graphique
- Enseignement du graphique, rapport au texte
- Text-based interface en Français
- Umberto Eco interface textuel : protestant (élitiste) / interface graphique : catho (populiste)
- Habitude au GUI par défaut ou CLI
- texte à l'écran perçut comme manque de design, Ex: Dwarf Fortress, avis des joueurs, 
- Lire la matrice
- Texte métaphore au même titre qu'un icône
- Espace de travail par défaut = bureau virtuel
- Le rapport à l'écrit dans les origine de l'interface graphique rapport à l'écrit

 


Le fait est que ce qui s'affiche sur les écrans des systèmes informatiques est divisé entre le graphique et le textuel.

La présence même minime d'éléments visuels hors d'une matrice de signes indique que nous sommes en présence de ce qui s'appelle un *environnement graphique* ou une *interface graphique* – de l'anglais GUI, graphical user interface. À l'inverse c'est lorsque que l'ensemble de ce qui nous est donné à voir est conditionné par une grille que nous nous trouvons devant un *environnement textuel*.

L'emploi du terme *graphique* – du grec ancien *graphein* (« écrire ») – en opposition à ce qui est de l'ordre du texte est contradictoire mais ce n'est pas un accident. 



<br>//////////////// début des notes 





Un environnement en mode texte (TUI, de l'anglais « Text User Interface », « Textual User Interface » ou encore «Terminal User Interface » est un rétronyme introduit dans le jargon informatique après l'invention des environnements graphiques pour se distinguer des interfaces en ligne de commande. 
https://fr.wikipedia.org/wiki/Environnement_en_mode_texte

le textuel à l'écran se définit par l'absence de graphisme ?
le textuel à l'écran est nostalgique ?


 



À l'écran *graphique* tend à signifier hors de la grille du texte.

La distinction environnement graphique, environnement textuel symbolisait la division entre les utilisateurs d'ordinateurs Mac — un système d'exploitation à interface graphique – et les utilisateurs d'ordinateurs compatibles MS-DOS – un système d'exploitation à interface textuelle.
En 1994 Umberto Eco décrit cette opposition comme un déplacement méconnu de l'éthique catholique et protestante.


>Je suis entièrement convaincu que le Mac est Catholique et le DOS Protestant. En effet, le Mac est contre-réformiste et a été influencé par le "ratio studiorum" des Jésuites. C'est un système gai, convivial, amical, il dit au croyant comment il doit procéder étape par étape pour atteindre - sinon le Royaume des Cieux - le moment où le document est imprimé@. C'est une forme de catéchisme : l'essence de la révélation est abordée au moyen de formules simples et d'icônes somptueuses. Chacun a droit au Salut. DOS est Protestant, voire Calviniste. Il permet la libre interprétation des écritures, réclame des décisions personnelles difficiles, impose une herméneutique subtile à l'utilisateur et tient pour acquis que tout le monde ne peut pas atteindre le Salut. Afin de faire fonctionner le système, il faut interpréter soi-même le programme : loin de la communauté baroque des fêtards, l'utilisateur est enfermé à l'intérieur de la solitude de ses propres tourments.[^1]

[^1]:UMBERTO ECO Comment voyager avec un saumon Nouveaux pastiches et postiches TRADUIT DE L'ITALIEN PAR MYRIEM BOUZAHER GRASSET Titre original : IL SECONDO D1ARIO MINIMO Éditions Bompiani, Milan, 1992 

Aujourd'hui l'interface type est *graphique*. Depuis 1995 l'environnement textuel MS-DOS devient invisible pour les utilisateurs car l'interface graphique Windows est directement exécutée au démarrage.

Cette « nouvelle guerre religieuse souterraine qui transforme le monde moderne » décrite par Eco prend alors des allures d'occupation, opposant la résistance textuelle à l'obéissance *graphique*. Ce qui n'est pas *graphique* – c'est à dire ce qui n'est pas convivial,

<br>//////////////// début des notes 



ce qui n'est pas nouveau ?`
 

 ce qui ne dit pas à l'utilisateur comment procéder étape par étape – sort de la normalité.



<br>//////////////// début des notes 



 L'user experience c'est ce qui est graphique ? 
 





<br>//////////////// début des notes 



 ## Pourquoi le texte dans un jeu c'est moche ? 
 





<br>//////////////// début des notes 



 debug
January 14, 2017 at 3:43 pm Reply
It’s a myth that somehow command line interfaces are inferior or outdated. If you develop or manage anything in IT a lot of your day is still spent at the terminal because it is so efficient and easy to extend.

Not saying I don’t use a UI for most of my music making of course, but for updating stuff via midi this is nice. 
http://www.synthtopia.com/content/2017/01/13/sendmidi-brings-midi-to-command-line/ 
 



Appliqué à ce qui est affiché d'un jeu vidéo, le terme *graphisme* n'exclut pas le texte mais le désigne en tant que plus petite unité d'un système destiné à en mesurer la qualité.

La forte présence du texte dans ce qui s'affiche d'un jeu vidéo est un mauvais signe. C'est un manque de graphisme, un mauvais graphisme, une paresse au mieux une absence volontaire de considération esthétique ou une nostalgie fétiche 

<br>//////////////// début des notes 



 un archaïsme 
 

. Une telle désobéissance tend à provoquer un sentiment de gène au joueur.



<br>//////////////// début des notes 



 trouver un commentaire sur texte comme d'un autre temps ? 
 



Au travers des commentaires vis à vis des *graphismes* du jeu Dwarf Fortress ce statut dépréciatif du texte apparaît comme les stigmates de cette dispute théologique décrite par Eco.

À la fois considéré comme *le meilleur jeu de tout les temps* de part la complexité et la profondeur de ses mécaniques il serait également le pire à cause de ses *graphismes* susceptibles de *faire saigner les yeux*. 

>[…]Laissez-moi commencer par dire que j'adore l'idée derrière ce jeu. Il est juste tellement....... FUN!
Le hic c'est que les graphismes du jeu font saigner mes yeux[…]
Donc, je vous le demande à vous amis redditeurs et joueurs de Dwarf Fortress, avez-vous connaissance d'un mode remplaçant les graphismes du jeu par d'autres plus propres ? […] Qui fonctionne parfaitement avec le jeu et permet d'avoir une meilleure interface graphique (plus intuitive, moins fatiguant)?
https://www.reddit.com/r/dwarffortress/comments/5ohf67/dwarf_fortress_with_better_graphics/?st=jas5klp5&sh=3a5a29ea

Dans Dwarf Fortress le joueur incarne une sorte de dieu architecte chargé de concevoir et d'administrer la cité d'un peuple de nains (nain au sens de la créature fantastique popularisée par J. R. R. Tolkien). 



<br>//////////////// déubt des notes 



 Ce qui est caractéristique de ce jeu c'est l'énorme quantité d'information disponible pourtant 
 


Aucune information n'oblige ou dirige le joueur quant à la manière dont il doit procéder.  
Les pouvoirs qui lui sont accordés semblent tout à la fois illimités et dérisoires. Il peut par exemple accéder à l'historique des pensées de chacun de ses sujets tout comme il accède à la composition minérale des sols. L'ensemble très exhaustif des informations contenues dans le jeu tend à être hiérarchisé de manière horizontale. 


<br>//////////////// début des notes 



 Une donnée en soi n'est pas plus précieuse qu'une autre, et son importance très relative est défini par le contexte, exemple : obtenir une information sur la composition minérale du sol est possible dès le démarrage de la partie et n'aura aucune importance jusqu'à ce que par exemple un autre village s'intéresse spécifiquement aux chaises en granite ? 
 


L'une n'étant pas plus précieuse que l'autre et ayant une influence sur la partie tout à fait contextuelle.

Composé en ASCII le jeu se donne intégralement à voir à travers une grille de glyphes monospace. Il apparaît comme un texte composé d'une suite aléatoire de caractères duquel émerge une image abstraite, texturée proche d'un *bruit de Perlin*. 

Sur cette grille chaque glyphe peut changer de signification en fonction de son contexte d'affichage et de sa couleur. 

Dans l'écran principal du jeu les glyphes s'organisent de façon à représenter la vue aérienne d'un paysage. 

Dans ce contexte, le caractère 'D' représente un dragon tandis que dans un écran de dialogue le 'D' représente la lettre 'D'. Ainsi le caractère '•' lorsque les glyphes sont organisés pour représenter la vue cartographique d'une région signale la présence de la source d'une rivière tandis qu'il représentera un rocher sur une vue aérienne.


<br>//////////////// début des notes 



 c'est l'inverse de l'user friendliness 
Quel est l'exemple de l'user friendliness 
 


Autrement dit, dans ce jeu l'utilisateur est libre interprète des écritures, il doit prendre des décisions difficiles. Afin de faire fonctionner le système, il doit interpréter lui-même le programme. C'est l'utilisateur protestant voire Calviniste qu'évoque Umberto Eco.

Dwarf Fortress Beyond Quality


<br>//////////////// début des notes 



 au delà de la qualité ? au delà du graphisme ? au delà de l'obéissance ? 
 



>Le joueur néophyte pourra être récalcitrant compte tenu de la charte graphique résolument rétro, et de l'interface maladroite du jeu. Pourtant, derrière ce côté Old School assumé se cache un bijou rare; La profondeur du gameplay, la grande immersion, la liberté totale du joueur et la richesse de l'univers compensent largement cette phase d'apprentissage ardue.
Le Wiki francophone de Dwarf Fortress http://www.dwarffortress.fr/wiki/index.php?title=Accueil

>Au bout d'une heure, peut-être une heure et demi, j'ai eu comme une révélation à propos des graphismes du jeu. Comme dans Matrix: "Je ne vois même plus le code, tout ce que je vois c'est des blondes, des brunes, des rousses…". D'un coup c'est devenu un lieu réel, avec des travailleurs s'agitant de partout, certains fatigués, assoupis à même le sol. Et je me suis senti mal de ne pas leur avoir fabriquer de lits. Laissez moi vous dire que c'était un moment très bizarre.
https://www.gamerswithjobs.com/node/1131786

Les témoignages de joueurs comparant la soudaine compréhension *graphique* du jeu avec une révélation sont nombreux. Ce phénomène s'est concentré autour d'une analogie avec une scène du film Matrix à propos d'un dialogue particulier.

>Cypher - Whoa, Neo. Tu m'as fichu la trouille!.
>Neo - Désolé.
>Cypher - C'est pas grave.
>Neo - C'est...
>Cypher - La Matrice? Ouais.
>Neo - Tu la regardes toujours codée ?
>Cypher - Bien obligé. Les décodeurs d'images travaillent pour le programme de construction. Mais il y a toujours trop de données pour décoder totalement la Matrice. On s'y fait. Je... Je vois même plus le code. Je vois partout des blondes, des brunes, des rousses. Hum, tu... veux un verre?
>Neo - Oui.



 



 plus que la normalité l'habitude définit l'usage, apprentissage de la lecture ? Langue étrangère ? 
Frigo sur le balcon, on ferme vite la porte comme sis le froid allais partir (adelina)

 





 



 ### L'user experience c'est ce à quoi on est déjà habitué ? 
 





 



 ## Pourquoi le texte c'est moche dans la vie ? 
 



Dans Matrix l'humanité est divisée entre les utilisateurs de la version *graphique* et les utilisateurs de la version *textuelle* de l'interface d'un même ordinateur appelé «la Matrice». 



 



 qu'est ce qu'un mode ? Voir tunning ?  
 



Pour l'humanité le mode *graphique* est le mode par défaut, normal.

Pour les utilisateurs du mode *graphique* le texte est invisible au démarrage, c'est à dire dès la naissance. Ils viennent au monde directement branchés à une version de la *Matrice* dans laquelle l'interface *graphique* est exécutée au démarrage. Leur expérience du système est limité à cet environnement qui constitue la forme de leur rapport social, détermine leur existence.

Cet environnement *graphique* récréé somptueusement l'impression de la cité états-unienne stéréotypée avec ses gratte-ciels et ses embouteillages. Il leur apparaît comme un système familier, rassurant, normal, qui les guide naturellement vers - sinon le Royaume des Cieux - le moment où débranchés de la *Matrice* leur corps est recyclé en nourriture pour les autres utilisateurs.



 



 Soleil vert, capital ? 
 



Contrairement aux utilisateurs du mode *graphique*, on ne naît pas utilisateur du mode *textuel*, on le devient même on est élu.
En effet il est nécessaire d'être contacté par un utilisateur qui nous initie à la vérité du mode textuel.



 




Il permet la libre interprétation des écritures, réclame des décisions personnelles difficiles, impose une herméneutique subtile à l'utilisateur et tient pour acquis que tout le monde ne peut pas atteindre le Salut. Afin de faire fonctionner le système, il faut interpréter soi-même le programme : loin de la communauté baroque des fêtards, l'utilisateur est enfermé à l'intérieur de la solitude de ses propres tourments.


on apprends dans le film que la matrice à été conçu dans le but de créer un monde dépourvu de souffrance ( c'est à dire dépourvue d'efforts, d'obstacles, un monde user-friendly ) 

association entre vérité et désolation apocalyptique – du monde – de la machine.
Version apocalyptique de la matrice est sa version textuelle.
L'apocalypse est la perte de contrôle ? La perte du langage, c'est l'autre, l'étranger ?
Vérité du monde = version apocalyptique, version non civilisée = non conviviale, sans guide/direction, sans dieu.
Vérité de la machine = version apocalyptique, version non civilisée = non conviviale, sans guide/direction, sans graphismes, textuelle.

01:31:38,565

Ne l'avez-vous jamais contemplée, n'avez-vous jamais été ébloui par sa beauté, son ingéniosité? Des milliards de gens vivent leurs petites vies dans l'ignorance. Saviez-vous que la première matrice avait pour dessein de créer un monde dépourvu de souffrances, où tous seraient heureux? Ce fut un désastre. Le programme n'était pas accepté. On a perdu des récoltes. Certains pensaient qu'on n'était pas à même de créer l'encodage de votre monde parfait. Mais je crois que votre espèce, Le genre humain, se satisfait d'une réalité faite de malheur et de souffrances. Le monde parfait était un rêve que votre cerveau primitif s'évertuait à fuir. C'est pour cela que la Matrice fut remaniée. L'apogée de votre civilisation. Je dis bien de votre civilisation car depuis que votre pensée est nôtre, c'est devenu notre civilisation, ce qui est le cœur du problème. L'évolution, Morpheus. Comme le dinosaure. Regardez par la fenêtre. Vous avez fait votre temps. L'avenir, c'est notre monde, Morpheus. L'avenir, c'est notre temps à nous.

01:33:25,372

Dieu n'existe pas, il n'y a pas de déterminisme

>Si, d'autre part, Dieu n'existe pas, nous ne trouvons pas en face de nous des valeurs ou des ordres qui légitimeront notre conduite. Ainsi, nous n'avons ni derrière nous, ni devant nous, dans le domaine numineux des valeurs, des justifications ou des excuses. Nous sommes seuls, sans excuses. C'est ce que j'exprimerai en disant que l'homme est condamné à être libre. Condamné, parce qu'il ne s'est pas créé lui-même, et par ailleurs cependant libre, parce qu'une fois jeté dans le monde, il est responsable de tout ce qu'il fait. L'existentialiste ne croit pas à la puissance de la passion. Il ne pensera jamais qu'une belle passion est un torrent dévastateur qui conduit fatalement l'homme à certains actes, et qui, par conséquent, est une excuse. Il pense que l'homme est responsable de sa passion.

>L'existentialiste ne pensera pas non plus que l'homme peut trouver un secours dans un signe donné, sur terre, qui l'orientera ; car il pense que l'homme déchiffre lui-même le signe comme il lui plaît. Il pense donc que l'homme, sans aucun appui et sans aucun secours, est condamné à chaque instant à inventer l'homme. Ponge a dit, dans un très bel article : “L’homme est l’avenir de l’homme.” C'est parfaitement exact. Seulement, si on entend par là que cet avenir est inscrit au ciel, que Dieu le voit, alors c'est faux, car ce ne serait même plus un avenir. Si l'on entend que, quel que soit l'homme qui apparaît, il y a un avenir à faire, un avenir vierge qui l'attend, alors ce mot est juste. Mais alors, on est délaissé.

« nous n’avons jamais été aussi libres que sous l’Occupation »

matrice sous forme graphique est programmée, objective.

matrice sous forme de texte est programmable, subjective.



 



 ### L'user experience c'est le catholicisme anti existentialiste ? 
 



 

