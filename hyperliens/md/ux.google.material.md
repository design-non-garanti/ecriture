 
## UX et Google Material Design

- Origine du matérial Design c'est le discours de l'UX qui se développe chez Google
- Enquête sur la Company Info 
- Description du material design, rapport au papier, accessibilité, métaphore
- Exemple d'enluminure biblique
- Otl Aicher 
- jamais les designers dont ils s'inspirent sont cités
- Extrême ressemblance entre logo dessiné par laszlo moholy et le logo de Google Material Design
- Material Design Actuel Logo = logo du bauhaus
- Nouvelle Typograhie de Tschihold 
- le sommaire des guidelines du material design est tellement exaustif qu'il décrit un interface


Depuis 2014 Google publie des directives destinées à la conception d'objets visuels, interactifs et animés. La stratégie étant d'unifier l'ensemble de leurs services tout en poussant les développeurs passant par la plate-forme de distribution de l'entreprise (Google Play) à respecter certaines règles d'ergonomie et d'apparence, Google ne soumettant pas les produits du “play store” à un processus de validation pour se différencier de l'App Store d'Apple.

Ce système porte le nom de “Material Design”. Il est officialisé le 25 Juin 2014 simultanément durant la Google I/O conférence et en ligne à l'adresse www.google.com/design/spec/material-design. C'est également à cette date que la page www.google.com/design apparaît.

Durant la conférence, le Material Design est défini ainsi par Jon Wiley :

« […] Nous nous sommes mis au défi de définir les principes physiques sous-jacents à nos interfaces pour élaborer un langage visuel qui synthétise les principes classiques de ce qui fait un bon design avec une solide compréhension des principes physiques les plus fondamentaux. Au début, nous avons pensé comme des designers, comment le présenter, quel apparence lui donner. Puis nous avons pensé comme des scientifiques, pourquoi se comporte il de cette façon ? Enfin après de nombreuses expérimentations et de nombreuses observations nous avons écrit tout ce que nous avons appris. C'est ça le principe du Material. […] » 

puis ainsi par Mathias Duarte :

« Le tout premier principe dans le Material Design est celui de la métaphore. […] Les métaphores sont vraiment puissantes car elles sont profondes de sens. Elles communiquent plus richement que le langage verbal le permet. […]
La métaphore est une forme de transmission de la connaissance qui dépend des expériences partagées, de fait cette capacité, la transmission de la connaissance et de l'apprentissage font parties des choses qui définissent l'humanité et de fait défini l'intelligence. Donc pour nous, l'idée de la métaphore est une histoire de fond pour le design. Elle unifie et façonne le design. Elle a deux fonctions, elle fonctionne pour notre audience, nous voulons présenter une métaphore qu'ils peuvent comprendre avec laquelle ils peuvent se connecter, qu'ils peuvent utiliser pour aller plus vite dans la compréhension de comment on utilise les choses. C'est aussi une métaphore pour nous-même, pour les designers et les développeurs et les project managers, et les gens de Quality assurance. Tout ces gens travaillent ensemble car quand vous avez une métaphore que tout le monde comprend, comprend intuitivement, vous n'avez pas à expliquer comment ils ont violé la sous-section C clause 2 de votre charte graphique, ils sentent tout simplement que c'est faux, hors-sujet. Donc pourquoi cette métaphore particulière, pourquoi avons-nous imaginé un matériau qui à une forme de papier suffisamment avancée pour se confondre avec de la magie, et bien d'une part il est évident que nous avons une grande expérience de la communication sur papier, elle est juste riche dans une histoire traversant l'ensemble de nos cultures de la transmission d'information et cela naturellement permet tant de différentes manières d'interagir avec. Mais l'autre aspect du papier est que c'est quelque chose de physique qui existe dans le monde, et cette idée que les surfaces parce qu’elles sont tangibles sont une métaphore que nous pouvons utiliser pour accélérer la compréhension de notre UI est vraiment importante. Vous avez cette perception des objets et des surfaces qui se passe dans les parties les plus primitives du cerveau, ça arrive dans ces cortex visuels qui sont dans la partie arrières et basses de votre cerveau et cela signifie simplement que … »


 comme un «langage visuel synthétisant les principes classiques d'une bonne conception, de l'innovation visuelle, les possibilité technologiques et scientifiques».

sur la page google.com/design/spec/material-design le Material Design est défini ainsi :

« Nous nous sommes mis au défi de créer un langage visuel pour nos utilisateurs qui synthétise les principes classiques d'une bonne conception (“good design”, dans le texte) avec l'innovation et les possibilité offertes par la technologie et la science. Ceci est le material design »

Pendant la conférence une personne demande pourquoi il y a des cercles en quantités, la personne dans une sorte d'ironie mêlée d'un sincère engouement s'excuse par avance en disant qu'elle est développeur et que sa question peux paraître bête pour un designer. Néanmoins cette personne demande de parler de ces cercles, où sont sont ils appropriés ? Que permettent ils de transmettre à l'utilisateur.

Mathias Duarte réponds à propos du travail de simplification qu'ils ont menés, la tentative de conserver uniquement des formes primaires élémentaires de «bas niveau» (dialectique de la raison). 
Que ce qu'ils font c'est de tout ramener aux formes géométriques les plus simple et basiques. Que nous pouvons y voir des cercles, des carrés, des rectangles, des divisions basiques de l'espace (structuralisme, bauhaus, nouvelle typographie). Qu'ils utilisent le cercle car naturellement il contraste avec l'espace qui est divisé, qu'il est un très bon moyen d'attirer l'attention de l'oeil sans effectuer de mouvements, qu'il peux indiquer une action primaire, qu'il créé du rythme par lui-même. Que le cercle est un outil à penser comme n'importe quel autre outil visuel. Que c'est une forme qui se propage de son point d'origine, qu'ainsi il correspond en termes d'affordance avec le toucher sur une surface et la propagation de sa force.(gestalt théorie)

Plus tard une autre question, la personne se présente comme UX UI designer, sa question concerne la manière dont s'animent les formes, il commence par poser un contexte (sans en faire la critique) où nous savons tous que le travail d'animation sert principalement à faire sortir du lot une application et à conserver sa popularité, que dans les agences de design les animations sont élaborés sur fireworks, after effects, origami ou même à la main, et que la principale des difficultés lors de l'élaboration d'une app est d'arriver à transmettre aux développeurs l'explication de comment on va interagir avec l'application et qu'il se demande si Google va fournir un outil permettant d'aider les designers à réaliser leurs animations, si dans les directives du guide que sera le material design il y aura en plus des explications sur les animations sur lequels se concentrer par défaut un outil permettant aux designers de transmettre aux développeurs les points nécessaires concernant les animations prévues …

Une autre personne pose la question des couleurs, son constat est le suivant on peux voir partout beaucoup de couleurs éclatantes pour ne pas dire criardes ces derniers temps, et elles se retrouvent partout dans la présentation, qu'il y a beaucoup de combinaisons colorées, comment ses couleurs vives sont-elles choisis dans le contexte du material Design, comment charment elles l'utilisateur, quel sens doit-on en tirer, et ne sont elles pas si brillantes qu'elles deviennent désagréables. 

La réponse immédiate Jon Wiley sous forme d'une blague entre collègues designers est qu'ils ont testés 41 nuances différentes. Puis Johnatan Lee explique qu'ils ont intentionnellement choisis des couleurs pour leur exubérance, afin de caractériser clairement les produits de la firme. Mais que à coté de cela les directives proposées concernant les couleurs ont été pensées pour être souple, de proposer des conseils d'associations de couleurs concernant des gammes colorées très diverses, même très discrètes afin que chacun puisse s'y sentir chez soi.

synthèse

Lors de sa présentation

Le Material Design est présenté comme un ensemble de directives destinées aux designers et développeurs d'interfaces graphiques. Les directives proposées insistent sur l'utilisation de formes géométriques élémentaires aux couleurs vives contrastées et complémentaires au comportement imitant de façon minimaliste les propriétés physiques du papier, de la page.

En tant que document le 

Le Document

Dans sa première version en 2014 le document se compose de 41 chapitres comme autant d'aspects définissant l'interface graphique en tant qu'objet, regroupés en 8 parties comme autant d'aspects définissant le travail de design d'interface graphique.

À la recherche du design perdu

En surfant via les archives de waybackmachine sur les pages indexés “corporate” sur le site de google, j'ai cherché à voir apparaître le terme design dans le discours de l'entreprise. Comment le material design à t-il été préparé, de quelle pensée provient-il ?  préparés cette  courant on distingue quatre phases moments 

Le contenu “corporate”

Sur le site internet d’un groupe ou d'une entreprise, consacrer un ensemble de pages à décrire factuellement la structure est une tradition. Ces pages sont généralement indexés sous le terme de “compagny”, “corporate”, (la société, l'entreprise), “compagny info”, “about” “about us” (à propos, qui sommes nous). Du prétexte, à première vue purement fonctionnel de l'exercice de description, se dégage l'occasion d'assurer l'authenticité d'un discours.

Comment se servir de ce contenu

De part sa duplicité, à l'échelle de sa formulation, le contenu même de ces pages n'est pas très scrupuleux et il serait hasardeux de s'en servir pour argumenter sur les réelles ambitions de l'entité qu'il concerne ou de son auteur. Contextualisé plus largement ; lorsque ce contenu évolue dans le temps, varie, s'adapte, est corrigé dans la durée. Apparaît à la fois ce qu'il a de régulier ou d'instable. Il est alors révélateur en ce qu'il fait trace d'une trajectoire, d'une direction ; d'une trame plus profonde : la structure d'un système, d'une stratégie.

 des dans la constitution d'une image, d'un discours, d'une pensée ce contenu devient révélateur 

Cette page parmi d'autres à pour objectif de renseigne
Il s'agit d'une énumération de faits

10 things Google has found to be true

“10 Things Google has found to be true” est un paragraphe sur la page “google today” sur le site google.com apparue en décembre 2001 sur l'adresse google.com/corporate/today.

En décembre 2001, la section “corporate information” est divisée en trois parties : “Profile”, “In Depth”, “The Difference”.

“Profile” contient les informations élémentaires, une section concerne le terme “google” et son origine, une section esquisse l'entreprise “en un clin d’œil” par le biais de statistiques ; nombre de requêtes quotidiennes : plus de 150 millions, nombres de pages web indexées : plus de 2 billions […] Google figure parmi les dix sites les plus populaires sur internet etc. Cette page contient également les noms et les fonctions des acteurs principaux de l'entreprise sous forme de crédits (on y retrouve notamment Jakob Nielsen annoncé comme représentant du Nielsen Norman Group en tant que conseillé technique). Une dernière contient les adresses des différents bureaux de l'entreprise ainsi que des conseils pour y accéder.


Le Material Design 

Savez-vous ce qu'est le Material Design ?
Décrire le Material Design n'est pas simple.

il est surpenant de constater la quantité d'articles ou de commentaires soulignant le caractère incompréhensible du Material Design, sous tout ses aspects, dans le discours, la théorie et la pratique.
Notamment sur ce qui le distingue du 'flat-design' ou du 'skeuomorphisme'


Material Design est un site internet consultable à l'adresse suivante https://material.io/.

En allant sur cette adresse nous accédons à la page index du site.

Le texte est en langue anglaise, la fonte utilisée est une monospace avec serif.

La page se divise en trois parties horizontales placées l'une au dessus de l'autre.

Les deux premières parties possèdent un fond d'un gris très sombre, proche du noir , et occupent 6 divisions sur 7 de la hauteur. Des marges latérales les ramène plus au centre. La dernière partie, beaucoup moins haute, possède un fonds très clair et occupe intégralement la largeur de la page. Elle forme une sorte de bande claire collée en bas de la page.

La première de ces parties tout en haut, comporte dans son premier tiers horizontal principalement du texte tandis que le reste de sa surface est occupé par des figures géométriques en mouvement permanent.

La deuxième partie, au centre, est divisée horizontalement en trois blocs verticaux dont l'essentiel de la hauteur est en hors-champ, en partie dissimulés par la bande  du bas de la page. Le premier bloc possède un fond gris sombre et comprend du texte. Le deuxième sur fond blanc comprend du texte et ce qui semble être le haut d'une forme géométrique. Le troisième sur fond jaune orangé comprend aussi du texte et ce qui semble être le haut d'une forme géométrique.

La dernière partie, la bande, est elle même verticalement divisée en deux bandes comprenant toutes deux une forme géométrique et du texte, le fond de la première est blanc, le fonds de la deuxième est d'un gris très clair.
Dans l'ensemble de la page, lorsque le texte est sur un fond sombre il apparaît blanc, lorsqu'il est sur un fond clair il apparaît du même gris très sombre que le fonds des deux premières parties. 

Dans la première partie, en haut à gauche de la page est inscrit en lettres capitales "MATERIAL DESIGN". Il s'agit du titre, du nom du site. Celui-ci est accompagné d'un symbole, un logo, positionné à sa gauche. Il occupe une surface équivalente à quatre de ses signes, deux de haut et deux de large. Ce symbole est constitué d'un triangle équilatéral blanc pointant vers le bas, superposé à un carré gris clair lui-même superposé à un cercle d'un gris plus sombre. Les trois figures s'assemblent en partageant des mesures équivalentes. Le coté supérieur du triangle et du carré se confondent, tandis que la pointe du triangle correspond au milieu du coté inférieur du carré, les quatre points du carré atteignent le périmètre du cercle sans le dépasser. Cette combinaison forme une sorte de 'M'.

Juste en dessous de ce titre il y a un petit paragraphe de quatre lignes 
"Material Design is a unified system that combines theory, resources, and tools for crafting digital experiences." Juste en dessous de ce paragraphe est inscrit une note, soulignée en petites capitales "WATCH THE VIDEO". Au survol de la souris le soulignement fais un léger mouvement vers le bas.<version>
Le titre et son logo, le paragraphe et sa note occupent la partie 
supérieure gauche de la page. La partie supérieure droit </version>

À droite Les trois figures du logo sont répétées et agrandies de façon à occuper un tiers de la hauteur et un peu plus d'un tiers de la largeur de la partie supérieure de la page. Tracées par un contour gris légèrement épais leur surface laisse entrevoir la couleur sombre du fond de la page. Le Triangle, le carré et le cercle du logo ne se superposent plus mais produisent une sorte de chorégraphie combinatoire. Les trois figures glissent horizontalement, parfois tour à tour, parfois au même moments. Échangeant leurs précédentes positions. Tandis que le cercle et le carré se contentent de glisser, le triangle procède en plus à des rotations inattendues. Il ne pivote jamais vraiment mais change de sens en déplaçant ses points le long des différentes mesures des autres formes. Parfois il épouse même entièrement la forme du carré en se rajoutant brièvement un quatrième point. Les trois figures arrêtent leur chorégraphie à intervalles réguliers, semblant choisir une position pour nous montrer pendant un cours moment un signe puis changent de nouveau leur configuration et reprennent une pause etc.
…

5 novembre 1999, google emploie le terme “design” dans la sous-rubrique “Google's Approach to Searching the Internet” en même temps que celui de “mission” (première sous-partie de l'article 
“Our Mission”).

La mission de google est d'organiser l'information mondiale, la rendre universellement accessible et utile.

L'objectif est de délivrer la meilleure expérience de recherche sur internet “World Wide Web”. Les avancées innovantes en matière de technologie de recherche doivent aider les utilisateurs “users” à trouver l'information qu'ils recherchent avec une pertinence et une facilité sans précédents.

La technologie de recherche est qualifiée d'innovante, le design d'interface-utilisateur lui est dit élégant.

L'interface est designée pour être élégante, facile à l'utilisation, propre, épurée pour mieux inscrire la recherche et en interpréter les résultats.

Les résultats sont délivrés par ordre d'importance et de pertinence.

La pertinence est calculée par la quantité de liens interdépendants sur une page, un lien allant de la page A à la page B est un “vote”.

L'honnêteté est pertinence, l'honnêteté est importante, c'est un élément quantifiable, la démocratie est objective car automatique.

La démocratie est un élément de conception, de design pragmatique.

Google est moral car il est démocratique et la démocratie est la morale.

La morale est quantifiable.

13 décembre 2001

À partir du 13 décembre 2001 la section compagny info du site de google se complexifie, la section “Google's Approach to Searching the Internet” s'appelle maintenant “Google Today” et le discours n'est plus tout à fait le même. Les 5 points décrivant les choix de google concernant les méthodes d'indexation du web – qu'il s'agisse des choix de design d'interfaces ou de la description de leur système de notation démocratique de sites – sont remplacés par «10 vérités», google ne se contente plus de décrire ses méthodes il en propose une, il propose des lois. En parallèle le terme “user experience” apparait pour la première ce qui dévoile la proximité des idées entre google et Donald Norman.

En 2008, la même année où Donald Norman avoue publiquement sa répulsion pour le terme “user” et explique préférer “people” citoyen, personne, indivu le 30 avril cette section est carrément renommée «design principles» c'est à dire «principes de design», et il ne s'agit plus pour google de se focaliser sur l'utilisateur mais sur l'individu “people” «we design for people we dont design for users» nous désignons des expériences et non des utilisations, nous designons des objets en sorte que leurs consommateurs en aient une expérience et non une prise en main, qu'ils n'en fassent rien si ce n'est recevoir leur message. 
En 2011 cette page est traduite en français et le terme de citoyen ou de personne ou d'individu n'apparait nullement, la page s'intitule 
«les utilisateurs avant tout» marquant encore plus cette difficulté de traduction du terme people, et le caractère peut-être exclusivement anglophone voir californien de l'utilisation du terme “people” concernant le design d'interface.

le 25 juin 2014 Google introduit sur son site une section design et dévoile son propre courant/discipline (discipline car ayant des disciples) de design, le “Material Design”. Le “Material Design” est une transposition graphique de leurs précédentes règles/philosophies/pensées, les mêmes arguments viennent alors commenter des formes, des mouvements, couleurs, choix de composition et de typographie. 
Le “Material Design” apparait comme une synthèse de ce dont le «citoyen» est habitué à observer sur son écran. Lors de ce dévoilement de nombreux articles établissent une parenté entre les deux grosses tendances graphiques ( en oubliant une troisième, le texte ) présentes sur le web c'est à dire le skeuomorphisme (voir les potentiomètres de Reason (jecrois) ou le micro de Apple) et le flat (sorte de style suisse international appliqué au web design) ce qui revient à la classique dichotomie figuration–abstraction et décrivent le “Material Design” comme la synthèse des deux modes de pensées, des mouvements s'ils existent. Pour ma part je pense que ce qui se joue dans le “Material Design” est plus proche du travail sur la norme telle qu'on pu le faire les suisses… à voir. Cette «méthode» de design s'appuie sur le matériau comme métaphore et propose d'envisager tout élément d'interface comme une matière «concrète» ayant des caratéristiques physiques (poids, texture, position, exposition à la lumière). Le matérial design se fait alors passer pour une sorte d'abstraction de la matière, métaphore abstraite qui serait la raison de son caractère innovant et unificateur, mais cette métaphore ne serait-elle pas simplement celle de la feuille de papier ?

1999
Sur google.com google parle d’user interface dans sa page compagny info.

2001
La page compagny info de google devient “google today” et google parle de “user experience” et affirme “focus on the user and all else will follow”.

2008
La page google today devient “design principles” et Google demande “focus on people – their work, their dreams”.

2014
La page “design principle” devient “design” et google invente le 
“Material Design” depuis l'unification du web dans sa forme 
et sa fonction s'accélère.

  ### L'user experience c'est un design marchand ?   
 