En 1991 l'informaticien Mark Weiser, publie l'article phare *The Computer for the 21st Century*[^1]
portant sur le concept d'informatique ubiquitaire (Ubiquitous Computing).
Weiser débute son article en soutenant que « Les plus profondes technologies sont celles qui
disparaissent.  Elles se lient dans le tissu de la vie quotidienne jusqu'à ce qu'elles ne se
distinguent pas ». Pour appuyer cette idée Weiser prend comme exemple l'omni-présence de l'écriture au
travers des livres, des enseignes de rue, des emballages de bonbons, etc. Selon lui « la présence
constante de ces produits de technologie littéraire ne nécessite pas d'attention active » : lorsque
nous regardons des panneaux de signalisation nous absorbons l'information sans effectuer «
conscieusement l'acte de lire ».  Pour Weiser l'informatique actuelle, quant à elle, est loin
d'avoir prit part à notre environnement, il la compare même avec la période où les scribes devaient
en savoir autant sur la fabrication d'encre et la cuisson de l'argile que sur l'écriture.  


L'informatique actuelle dont parle Weiser est celle des ordinateurs personnels. En effet, le concept
d'informatique ubiquitaire, porté par le programme de recherche du même nom initié en 1988 au Xerox
Palo Alto Research Center (PARC) par Weiser et ses collègues, est décrite comme la troisième phase
de l'informatique, chaque phase décrivant des relations différentes entre utilisateurs et
ordinateurs. La première (≈ années 1960, 1970) est la phase des ordinateurs centraux (mainframe) :
plusieurs personnes partagent un seul ordinateur. La deuxième (≈ années 1980, 1990) est celle des
ordinateurs personels : une personne,  un ordinateur.  L'informatique ubiquitaire décrit un
troisième paradigme où une personne est entourée de plusieurs ordinateurs qui disparaissent à
l'arrière plan [^2]. Le programme de recherche sur l'informatique ubiquitaire est au départ envisagé
comme une « réponse  à ce qui n'allait pas avec l'ordinateur personel[^6] ». Celui-ci est considéré
comme trop gourmand en attention, au contraire l'informatique ubiquitaire envisage de rendre
l'ordinateur « si intégré, si ajusté, si naturel, que nous l'utilisons sans même y penser[^7] ».
