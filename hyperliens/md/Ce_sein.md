>Couvrez ce sein que je ne saurais voir : Par de pareils objets les âmes sont blessées, Et cela fait venir de coupables pensées.[^1]

[^1]: Molière, *Tartuffe, ou l'imposteur*, 1664, III, 2
