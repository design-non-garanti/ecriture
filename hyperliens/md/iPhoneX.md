Apple, après en avoir « toujours rêvé », a enfin « créer un iPhone qui ne soit qu'écran ». Le
home bouton, dernier bouton sur la face de l'iPhone où les yeux de l'utilisateur se portent, a
finalement disparu afin de laisser place a un iPhone « si immersif qu'il s'efface ».  Il « ne
requiert aucun câble de chargement » laissant place au chargement sans-fil, il suffit de déposer son
téléphone sur une station de chargement, l'acte de recharger se confond alors avec celui de
déposer son téléphone sur une table de nuit, (fondamentalement il y a toujours un câble mais
celui-ci est caché derrière celle-ci). Le téléphone se recharge sans avoir à effectuer
« conscieusement l'acte de » le brancher, suggérant un dispositif qui ne nécessite même plus d'être
recharger en énergie. Si cette tendance ne tient pas qu'à Apple, les éléments de communication de la
marque s'inscrivent parfaitement dans la vision de Weiser. 
