  
**Couvrez cet objet technique que je ne saurais voir**

- pourtant de plus en plus présent l'ordinateur est paradoxalement en train de disparaître
- au travers des discours, des formes, des usages
- ordinateur en tant que système programmé disparaît 
- cette disparition entraîne avec elle un contrôle des usages, une innatention, une inconscience,
  une fermeture, un déterminisme, une dissimulation, une perte d'action, une mainmise

Au nom de l'efficacité ou de la simplicité l'ordinateur pourtant annoncé comme de plus en plus présent  est paradoxalement en train de disparaître. 

 