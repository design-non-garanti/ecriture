 
- Considération de l'objet technique seulement comme utile et fonctionnel service
- Les idolâtres de la machine présentent en général le degré de perfection d'une machine comme
  proportionnel au degré d'automatisme. 
- Dépassant ce que l'expérience montre, ils supposent que, par un accroissement et un
  perfectionnement de l'automatisme on arriverait à réunir et à interconnecter toutes les machines
  entre elles, de manière à constituer une machine de toute les machines.
- Or, en fait, l'automatisme est un assez bas degré de perfection technique.
- Simondon soutient que « pour rendre une machine automatique, il faut sacrifier bien des
  possibilités de fonctionnement, bien des usages possibles ».
- Pour Simondon, ce qui élèverait le degré de technicité d'une machine, ne technicité, correspond
  non pas à un accroissement de l'automatisme, mais au contraire au fait que le fonctionnement d'une
  machine recèle une certaine marge d'indétermination. C'est cette marge qui permet à la machine
  d'être sensible à une information extérieure. 
- Pour Simondon  une machine Une machine purement automatique, complètement fermée sur elle-même
  dans un fonctionnement prédéterminé, ne pourrait donner que des résultats sommaires. 
- suppose l'homme comme organisateur permanent, comme interprète vivant des machines les unes par
  rapport aux autres.
 