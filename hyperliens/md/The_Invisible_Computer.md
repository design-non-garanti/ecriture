L'un des objectifs majeurs de *The Invisible Computer* est « d'accélérer le jour où la
technologie de l'ordinateur disparaisse hors de la vue et que la nouvelle technologie qui la
remplace est aussi accepté et facile à utiliser qu'un magnétophone à cassette ou qu'un lecteur
CD » -> transformer les ordinateur en information appliance

l'ordinateur actuel est perçut comme un problème :

>La technologie actuelle est envahissante et autoritaire. Elle nous laisse sans moments de silence,
>avec moins de temps pour nous, avec un sentiment de perte de contrôle sur nos vies. 
>Nous sommes pris au piège dans un monde créé par des technologues pour des technologues.


L'ordinateur personel est perçut comme la plus frustrante des technologies.
L'ordinateur devrait être considéré infrastructure.
L'ordinateur devrait être silencieux, invisible, discret, mais il est trop visible, trop exigeant. Il contrôle notre destin.

Infrastructure = hors de la vue, dans notre environnement, dans le background mais à la fois ce sur
quoi on repose et on est dépendant


>the general purpose computer is a great compromise, sacrificing simplicity, ease of use, and
>stability for the technical goals of having one device do all.
>The exciting new services are social and interactive, yet here we are, trying to build them on a
>device whose first name is "personal."
>Today's PC has gotten too big, too expensive, too complex, 
>demanding more and more attention. It is a general purpose machine, which
>means it can do anything. This is not a virtue.
>We need to move to the third generation of the PC, the generation where
>the machines disappear from sight, where we can once again 
>concentrate upon our activities and goals in life.



Afin de changer cette complexité
Norman propose les informations appliances
user-centered, human-centered
où la technologie de l'ordinateur disparaît derrière la scènes


Toutes les méthodes sont bonnes, retiré le mot ordinateur fait partie de la disparition


Norman prenant l'exemple des moteurs électriques, présents par dizaine dans les maisons et cachés à
l'intérieur des horloges, ventilateurs ou mixeurs, précise que l'absence du mot moteur dans le nom
de ces objets contribue à rendre les moteurs qui permettent leurs fonctionnements invisibles.
Quelques paragraphes plus tard, il transpose cette idée à l'ordinateur.  Si les ordinateurs sont de
plus en plus intégrés dans les objets tel que les micro-ondes ou les machines à café, de la même
manière que pour les moteurs, lordque le terme ordinateur n'apparaît cela contribue à son invisibilité.
