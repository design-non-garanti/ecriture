# L'ordinateur du XXIe siècle 

Les technologies les plus profondes sont celles qui disparaissent, celles qui s'entremêlent dans le tissu de la vie quotidienne jusqu'à s'y confondre. 

Songez à l'écriture, peut-être la première technologie de l'information. Cette technologie fameuse pour avoir libéré l'information des limites de la mémoire individuelle en nous donnant la capacité de représenter symboliquement le langage parlé et de le stocker à long terme. L'écrit est ubiquitaire. 


<!-- Bien sûr --> Les livres, les magazines et les journaux transmettent <!--de--> l'information écrite tout comme les <!-- mais aussi les --> panneaux de signalisation, les panneaux publicitaires, les enseignes et devantures de magasins, les graffitis. Les emballages sont recouverts d'écriture. La présence constante de ces produits <!-- issus --> des technologies de l'écrit ne nécessite pas une attention active, mais pourtant l'information à transmettre est prête à être reçue en un coup d'œil. Il est difficile d'imaginer la vie moderne autrement.

À l'image de l'écrit, les technologies de l'information à base de silicium – les ordinateurs – font parti intégrante de l'environnement. L'importance de l'ordinateur ubiquitaire est comparable à celle de l'invention de l'écriture. <!-- GR propose : «dans les pays développés --> Quiconque ne possède pas d'ordinateur <!-- personnel --> a au moins en poche un smart-phone ou chez lui une tablette posée au hasard sur une table basse ou un canapé. <!-- GR propose : «Maintenant» --> Les ordinateurs sont présents dans les interrupteurs, les thermostats, les chaînes stéréo, les fours. Ces *objets connectés* sont d'innombrables petits appareils programmés présents dans l’électroménager, les installations publiques, divers guichets, caméras, bornes, capteurs etc. Ils permettent d'activer le monde. La plupart des ordinateurs sont <!-- en fait, GR «devenus» ou ' '? --> invisibles aussi bien dans la réalité que dans la métaphore. Ces machines sont interconnectées dans un réseau ubiquitaire ; on n'y pense même plus et cela va de soi, l'ordinateur est largement ordinaire.

Souvent emprunté à la langue anglaise, aux mathématiques ou aux sciences, son jargon est celui de la vie courante : *data*, *mails*, *likes*, *algorithmes*, *crowndfunding*, *crowdsourcing*, *cloud*, *hashtag*, *coworking*, *e-commerce*, *data-center*, *interaction*, *visualisation*, *tags*, *smart university* etc. La situation est peut-être comparable à une époque où la population largement rurale et imprégnée de son environnement en savait autant sur les plantes et les animaux. Plus important encore, l'omniprésence des ordinateurs aide à surmonter le problème de la surcharge d'information. <!-- GR tombe dans le panneau : « la provoque et l'intensifie surtout ! -->  Il y a autant d'informations à la disposition de nos sens lors d'une ballade dans les bois que dans n'importe quel système informatique. Certaines personnes trouvent même que l'utilisation de l'ordinateur est aussi relaxante qu'une promenade parmi les arbres. Les machines sont adaptées à l'environnement humain, elles ne forcent pas les humains <!-- GR : «réfractaires» (pas mal) --> à entrer dans le leur. Des centaines d'ordinateurs dans une pièce peuvent sembler intimidants au premier abord, tout comme des centaines de volts circulant à travers les fils dans les murs ont pu le faire. Mais comme les fils dans les murs, ces centaines d'ordinateurs sont invisibles aux yeux de tous. Nous les utilisons inconsciemment pour accomplir nos tâches quotidiennes. Cette informatique ubiquitaire est graduellement devenue le mode dominant d'accès aux ordinateurs au cours des vingt dernières années. Comme l'ordinateur personnel, l'informatique ubiquitaire ne permet rien de fondamentalement nouveau, mais rend tout plus rapide et plus facile à vivre. <!-- GR tombe dans le panneau : « discours positiviste … depuis la diffusion massive des smartphones et ordis portables (?) --> Avec moins de tension et de gymnastique mentale, elle transforme les possibles. La publication assistée par ordinateur, par exemple, n'est pas fondamentalement différente de la composition typographique classique ou de la photocomposition. <!-- GR (dur à lire) : «les utilisateurs changent? et transforment? chacun en "secrétaire-imprimeur-editeur"» --> Mais la facilité d'utilisation fait toute la différence. Ce commentaire des principes de l'informatique ubiquitaire retrace ce qu'est la vie dans un monde plein de gadgets invisibles. Pour appuyer cette présentation je propose de revenir par le biais du récit sur cet ensemble élaboré qui constitue notre quotidien  :

Michel se réveille : il sent l'odeur du café. Il y a quelques minutes, son *iGoogle©* – équipé de l'application *iPresso : quoi d'autre ?©* – alerté par l'agitation caractéristique de l'éveil, lui avait discrètement demandé <!-- : -->
>Café?
et il avait marmonné 
>Oui.

Michel regarde son quartier par la fenêtre. Sur son *iGoogle©* s'affiche une carte indiquant les itinéraires de ses voisins, reliés en tant que *iPotes* à l'application *coLiving©*. Comme tous les jours ces trajets semblent très similaires au sien, ça le rassure.

Au petit-déjeuner Michel consulte les actualités, il glisse machinalement son doigt sur l'écran de son *iGoogle©* pendant quelques minutes et fait défiler environ une centaine d'articles. Il n'en lit aucun mais prend soin de partager les articles qui affichent le plus de votes positifs pour le moins de vues afin de trouver quelque chose à dire à ses collègues de start-up (reliés en tant que *workPotes©*).  

Il reçoit un message sur son *iGoogle©* c'est l'entreprise *smartCar©* qui a fabriqué son ouvre-porte de garage. Il a perdu son manuel d'instruction <!-- GR : « pour la pose (?) --> pour la pose de cette dernière et leur a demandé de l'aide. Leur réponse est la suivante : « Les produits *smartCar©* sont si simples d'utilisation qu'ils ne sont pas livrés avec un manuel d'instruction. Grâce à leur design intuitif, n'importe qui peut les installer en un clin d'oeil ;). Néanmoins suite à votre demande, pour plus de confort et de sécurité, nous avons automatiquement pris rendez-vous avec un technicien expert qui viendra vérifier votre produit *smartCar©*. Nous avons sélectionné le créneau horaire idéal en nous basant sur votre *iGoogleAgenda©*. Merci de votre fidélité et passez un smartDay :) »

<!-- Dans son *smartCar©* qui -->
<!-- GR --> Dans ce *smartCar©* qui le conduit sur le chemin du co-working, Michel actualise son profil *coLiving©* avec un commentaire sur sa situation : « j'ai cru qu'il y avait encore besoin d'un manuel pour la porte d'un garage lol #smartCar ». Tout *feedback* – aussi succinct soit-il – est précieux pour le bon design d'un produit et ajoute en échange des points de fidélité au compte *smartShop©* de son *iGoogle©*. Tandis que son *smartCar©* roule automatiquement sur le trajet le plus populaire des trajets les plus courts, il ne remarque pas la jeune joggeuse et le vieil homme obèse qui traversent illégalement la route juste devant son véhicule-intelligent ! **Heureusement <!-- GR : « Évidemment » --> celui-ci évite soigneusement la joggeuse grâce à son système de reconnaissance *moralDriving©*.**

Son *smartCar©* trouve instantanément la meilleure place de parking disponible, la mieux notée des places parmi les plus proches de l'entrée. Michel arrive au co-working. Alors qu'il entre dans le bâtiment, il est automatiquement enregistré. Pour plus de confort et de sécurité et pour ne pas perdre en productivité, le simple fait de rentrer dans le bâtiment <!--GR-->sert de signature des conditions d'utilisation des données. En chemin, il s'arrête dans les bureaux de quatre ou cinq *workPotes©* pour échanger des salutations et des nouvelles. Eux aussi ont soigneusement épluché leur fil d'actualité au petit-déjeuner et il est bien agréable d'échanger sur les sujets les plus populaires du moment. 
<!-- GR : « +Exemple ? » --> 

La revue *Technologie pour le peuple, «le ressenti avant tout»* viens de publié un article partagé 1321 fois *topTrending* intitulé : « L'IA est la nouvelle IU », qui explique l'importance des intelligences artificielles dans la simplification des interactions naturelles pour l'ère de disparition technologique à venir.
>Avec la bonne intégration de l'intelligence artificielle au sein des applications développées par les start-up innovantes, les clients n'ont plus besoin de comprendre la technologie pour l'utiliser, aussi compliquée soit-elle. Ils ont juste à parler, toucher ou faire des gestes à l'intelligence artificielle qui contrôle leur *iGoogle*.

<!-- https://www.accenture.com/us-en/insight-artificial-intelligence-ui, consulté le 17-12-20 -->

Michel jette un coup d'œil à la fenêtre : une journée grise dans *l'inovallée*. Son *iGoogle©* indique 75 % d'humidité et 40 % de chance de se prendre la pluie <!-- GR : «rencontrer» --> dans l'après-midi. Pendant ce temps, c'est une matinée tranquille au bureau. Habituellement, l'application *iGoogleWork©* signale au moins une réunion spontanée urgente par matinée. À la place sur son *iGoogle©* s'affiche le commentaire d'un *workPote©* qui a voté contre son dernier profil *coLiving©* et a ajouté un : « Mais tout le monde sait que ça n'existe plus les manuels d'utilisation #BOLOSS ».
Il choisit de jouer à *angryCandy©* sur son *iGoogle©* mais il en connaît d'autres qui auraient surenchéri : habituellement des gens qui ne reçoivent jamais de commentaires, mais qui veulent simplement se sentir impliqués.  

Le témoin près de la porte auquel s'est abonné Michel via l'application *activateWorld©* clignote : le café est prêt. Il se dirige vers la machine à café.

De retour à son bureau, Michel ouvre l'application *coWork©* et *poke* son <!-- GR : « via » --> *workPote©* Michelle du groupe *design*, avec qui il partage un bureau virtuel pendant quelques semaines. Ils travaillent ensemble sur un projet innovant *topTrending*. Le partage de bureaux virtuels peut prendre de nombreuses formes - dans ce cas, les deux parties se sont mutuellement données accès à leurs détecteurs d'emplacement, ainsi qu'au contenu et à l'emplacement de leurs *iGoogle©*. Pour plus de sécurité *iGoogle©* conserve toutes les données. Michel choisit de mettre en miniatures sur son *iGoogle©* ce qui s'affiche sur le *iGoogle©* de Michelle. Il n'y prête pas attention, mais il se sent d'avantage impliqué en co-working lorsqu'il remarque que les miniatures changent du coin de l'œil ; il peut facilement agrandir si nécessaire. 

Une notification apparaît sur l'*iGoogle©* de Michel indiquant que Michelle veut discuter avec lui à propos de la section *guidelines* du rapport d'activité *inovallée2028*.
D'un seul clic Michel accepte et le visage de Michelle s'affiche sur son *iGoogle©*.

« J'ai lutté avec ce troisième paragraphe toute la matinée et il y a toujours quelques problèmes de rédaction qui donne une mauvaise tonalité à l'ensemble :( ; tu veux bien y jeter un coup d'oeil ? »

« Pas de problème :) »

Michel s'assoit et analyse le paragraphe grâce à l'application *textExperience©* de son *iGoogle©*. Un mot est automatiquement surligné en rouge : signe qu'il produit une expérience de lecture anormale. Michel en touchant le mot sur l'écran de son *iGoogle©* transmet l'information à Michelle, qu'il commente :

« Je pense que c'est ce terme «ubiquitaire». Il n'est tout simplement pas assez commun, et ça donne à l'ensemble un ton institutionnel. Peut-on reformuler la phrase pour s'en débarrasser ? <!-- Le remplacer par naturel ou intelligent par exemple --> »

"Je vais essayer ça. Au fait, Michel, tu as eu des nouvelles de Marie Hausdorf?"  

"Non. Qui est-ce?"  

"Tu te souviens, elle était à la réunion spontanée urgente de la semaine dernière. Elle m'a dit qu'elle allait te contacter."  

Michel ne se souvient pas de Marie, mais cette réunion spontanée urgente lui dit quelque chose. 

Grâce à l'application *iRemember©* il recherche rapidement dans son *cloud©* à partir des *tags* : /rencontre /deux dernières semaines /plus de 6 personnes /nouvelle personne. Différents noms apparaissent dont celui de Marie. Comme c'est souvent le cas dans les réunions spontanées urgentes, Marie a mis à la disposition des autres participants un lien public sur son *profilPro©*.

Son *profilPro* semble très similaire au sien, ça le rassure. Il enverra un mot à Marie pour voir ce qui se passe. Michel est bien content que Marie rende accessible publiquement son *profilPro©*, beaucoup de gens n'ont pas ce réflexe…  

Dans notre <!-- Gr : «le» --> monde de l'ordinateur ubiquitaire, les portes ne s'ouvrent qu'aux bons porteurs de badges, les chambres accueillent les gens par leur nom, les réceptionnistes savent réellement où se trouvent les gens, les ordinateurs récupèrent les préférences de ceux qui les utilisent, et les agendas de rendez-vous s'écrivent tout seuls.
L'agenda automatique montre comment une chose aussi simple que de savoir où se trouvent <!-- Gr : les abonnés || adhérents || utilisateurs, Mb : les citoyens || le peuple --> peut entraîner à la connaissance profonde d'une situation.
Révolutionner l'intelligence artificielle, a d'abord consisté à faire entrer les ordinateurs dans la vie quotidienne. 

Dans l'informatique ubiquitaire deux points sont d'importance cruciale: l'emplacement et l'échelle. Il n'y a rien de plus fondamental pour la perception humaine que la géolocalisation, et les ordinateurs ubiquitaires savent où ils se trouvent. Un ordinateur sachant simplement dans quelle pièce il se trouve, adapte son comportement de manière significative c'est ce que l'on nomme l'intelligence artificielle.

De telles machines on fait de l'informatique une partie intégrante et invisible de la façon dont <!-- les citoyens || le peuple --> vivent leur vie. Les ordinateurs sont présent dans le monde comme un arrière-plan de l'environnement humain naturel. 

Cette disparition est une conséquence induite non pas par la nature de la technologie, mais par celle de la psychologie humaine. 

Nous n'avons pas conscience des pratiques ou des objets auxquelles nous sommes habitués.

Lorsque vous regardez <!-- un panneau de signalisation || un panneau publicitaire -->, par exemple, vous en absorbez l'information sans l'avoir consciemment lu.

Le philosophe Michael Polanyi l'appelle la "dimension tacite"; 

le psychologue TK Gibson parle d'"invariants visuels"; 

les philosophes Georg Gadamer et Martin Heidegger évoquent "l'horizon" et le "à portée de main", 

Pour le chercheur John Seely Brown il s'agit de la "périphérie". 

Ce constat nous rappelle qu'il est nécessaire que les objets disparaissent pour que nous soyons libres de les utiliser sans réfléchir et pouvoir ainsi nous concentrer sur de nouveaux objectifs.

  fin du détournement  

Le texte qui précède à été réalisé pour ce mémoire.
Il s'agit d'un détournement-traduction de l'article « Un ordinateur pour le XXIe siècle » écrit par Mark Weiser en 1991. En anglais « The computer for the 21st Century », ce texte est considéré comme fondateur de la notion d'informatique Ubiquitaire. Il n'existe pas en langue Française.

Dans l'original Mark Weiser idéalise l'informatique ubiquitaire future, c'est une critique de l'informatique de son présent.

Dans le *palimpseste* que vous avez lu, c'est l'informatique ubiquitaire de notre présent qui est idéalisée. Cette candeur se veux celle d'un regard naïf sur l'informatique.

Nous sommes en 2017, vingt-six ans plus tard, le futur est là, force est de constater que l'informatique idéale de Weiser – un réseau interconnecté de *gadgets invisibles* – semble avoir été réalisé en grande partie.

L'internet des objets* devrait combler les parties manquantes.

L'informatique ubiquitaire est décrite par Mark Weiser comme l'avenir naturel de nos machines, on peux néanmoins se demander si il s'agit vraiment d'une prophétie ou d'un projet. 

L'histoire de l'informatique de ces 20 dernières années est-il le produit de notre inconscient ou une direction donnée par des décisions individuelles conscientes ? Serait-ce (par hasard) le résultat d'un design ? 

>Le monde de l'informatique est un monde imaginaire constitué de choses arbitraires qui ont toutes été flanquées là par quelqu'un à un moment donné. Tout ce que nous pouvons en voir a été conçu, designé par des individus mais lorsque ces choses nous déplaisent nous blâmons la technologie.(Nelson)

Michel vit dans un monde *pharmakon* qui empoisonne avec des remèdes. Weiser oubli de préciser un détail important : Sal dans la version originale ou Michel dans celle-ci n'est pas prioritaire des ordinateurs qu'il active. Cherchez l'erreur, où sont les ordinateurs dans la journée de Michel ? Ce sont les noms de produits copyrightés. Ils sont effectivement invisibles, il ne sait pas qu'il en active les logiciels. Il les active, on utilise pas ce dont on ignore l'existence. Quand bien même il aurait conscience de leur présence, ils ne sont pas reprogrammables, quand bien même ils seraient programmables, Michel ne sait pas programmer.

  
En 1991 Mark Weiser dirige le Computer Science Laboratory au Xerox PARC (Parc pour Palo Alto Research Center), une division de l'entreprise Xerox – fabricants d'imprimantes – consacré à la recherche et au développement.
L'objectif de cet article était de vulgariser la notion d'informatique ubiquitaire pour le néophyte.

Il s'agit également d'une remise au goût du jour, malgré 

Cette réécriture permet de soulever 



 est présentée comme un projet pour le futur. cette notion d'informatique ubiquitaire est présenté comme  texte


Il s'agit d'une remise au goût du jour d'un des articles phares sur l'informatique ubiquitaire, un concept 



Ce célèbre texte de Weiser publié en 1991 détourné et remis au goût du jour décrit la situation actuelle plutôt que celle envisagée est espérée par Weiser lors de sa publication




il est tellement fasciné par cette idée qu'
il confond habitude et conditionnement.
Il confond information et perception, c'est le principe de l'user experience.

Tel un Sisyphe heureux poussant son rocher au sommet pour qu'il redescende.

Il parlant d'habitude de présence 
il oublie completement l'usage, puisqu'il décrit l'écriture comme naturelle.
Il oublie telement l'usage qu'il oublie qu'il faut toujours apprendre à écrire après 10000
Ce texte décrivant largement notre rapport l'ordinateur actuel
Il pas confusion entre habitude et conditionnement
Ne rapport à l'ordinateur est sur-teinté du monde du travail de bureau, du travail en col blanc.
il parle de l'ordinateur uniquement dans un contexte d'entreprise 
Présence des marques dans l'histoire de michel

l'ordinateur ubiquitaire à été pensé avec l'écrit comme modele, dans ce contexte l'écrit à été pensé comme outil de communication, non comme objet esthétique ou poétique
écrit comme outil de communication  l'écrit définit comme outil de représenter le langage parlé, c'est en réalité un type d'écrit très particulier ? les exmples pris de l'écrit ne sont que des exemples d'écrits à lire (on pourrait se demander s'il n'évoque pas sans le dire des supports publicitaires, signalétiques, écrit informatif, qui fais loi ou de l'écrit à consommer) (déjà écrit) et non des supports / outils d'écriture, l'omniprésence de l'écrit à lire. L'omniprésence de l'ordinateur à lire, l'omniprésence de l'ordinateur comme objet de consommation ou comme outil mercantil

société de l'ordinateur société du contrôle, communication comme outil de contrôle (cybernétique, l'art du contrôle des systèmes, tout est système, tout est contrôlable)

et la diffusion de l'écrit ? Pour Weiser, le medium c'est le medium.

Social credit system 




Il parlant d'habitude de présence 
il oublie completement l'usage, puisqu'il décrit l'écriture comme naturelle.
Il oublie tellement l'usage qu'il oublie qu'il faut toujours apprendre à écrire après 10000
Ce texte décrivant largement notre rapport actuel à l'ordinateur ne confondons nous pas habitude et conditionnement dans notre rapport à l'ordinateur ?



teinté de blanc, celui des cols des travailleurs de bureau
(Ce célèbre texte de Weiser publié en 1991 détourné et remis au goût du jour décrit la situation actuelle plutôt que celle envisagée est espérée par Weiser lors de sa publication)



Déjà Weiser  présente un projet parlant de l'ordinateur uniquement dans un contexte bureaucratique, industriel)

traduction 
re-contextualisée, parodique

à une époque il y avait tout intérêt à ce que le peuple ne sache pas lire (comme pas coder aujourd'hui) 

ex disparition/transformation des récits oraux.

dev startupers d'appli sont comme des moines copistes, ils reproduisent le code ou la bonne manière de coder, commentent, notent «innovent» mais jamais ne transgressent

Weiser très prophétique mais et très à coté.
inversion -> en 1991 c'est les bizarres (riches, métiers très spécialisés, l'élite) qui téléphonent et qui passent pour des bizarres dans la rue pareil pour les ordinateurs, on a inverser ça dans le texte, comme c'est inversé par rapport à son texte dans la vrai vie.

ars memoria

Weiser, l'ordinateur une pensée moderne

aventure de l'ordinateur civile, aventure de la consommation et du commerce
 