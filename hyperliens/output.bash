#!/usr/bin/env bash

# -c styles
# -H in header
# -B before body
# -A after 

for f in md/*.md
    do
        name=$(basename "${f%.*}")
        echo "$name".html
        pandoc -c ../document/styles.css $f -o html/"$name".html
    done

