#!/usr/bin/env bash

# -c styles
# -H in header
# -B before body
# -A after 

rm html-not-standalone/*

for f in md/*.md
    do
        name=$(basename "${f%.*}")
        echo "$name".html
        pandoc $f -o "html-not-standalone/$name".html
    done

a="all/all.html"

rm $a

echo "<html>
    <head>
        <title>TITLE</title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="../document/styles.css" type="text/css" />
    </head>
    <body>
" >> $a

for f in html-not-standalone/*.html
    do
        name=$(basename "${f%.*}")
        echo "" >> $a 
        echo "" >> $a 
        echo "" >> $a 
        echo "<h1>$name</h1>" >> $a
        cat $f  >> $a
    done

echo "</body></html>" >> $a

