
## Console de Facebook

- Affirmation dans le message
- Invoquer la peur et prétexter la sécurité de l'utilisateur
- Ne pas explorer les dessous du web (le code informatique)
- Console comme outil de développement, de compréhension
- Self-Xss attaque
- Console comme espace de manipulation pour l'utilisateur amateur (non prévue, non proposé à
  l'origine par Facebook alors que cela serait possible)
- Ex: Nick Briz Why / How to Leave Facebook empowerment
- Dépendance, intérêt à garder ses utilisateurs et ne pas les laisser exporter leurs données
- Aaron Swartz import / export
- Raccourçi chez Certeau

## Keynote iPhone 1 

- Messe Apple (musique, prédicateur, logo pomme avec lumière, précher, etc.)
- Apple and the goal of preaching, définir les besoins https://www.9marks.org/article/steve-jobs-and-the-goal-of-preaching/
- Apple ne présente pas l'iPhone comme un ordinateur 
- "Dropping the Computer from the name" (disparition psssh)
- Faire circuler une certaine image de l'iPhone
- The power of "I"
- Easy / Smart solution

## Yellow Dots 

- Relation entre gouvernements et industriel
- But original contrefaçons de documents, assurance, protection
- Puce dans l'imprimante juste avant l'impression
- Pas requit d'informer l'utilisateur
- Ex: The Intercept NSA document
- Espace Imprimé
- Auteur, impossibilité de mourrir

## App Store modèle de distribution / contr'ole du développement logiciel

- discours iphone pas un ordinateur dans la keynote
- discours de steve jobs qui dit que faut pas laisser n'importe qui faire tout qu'est qui veut sinon
  ça va tout casser
- discours basé sur la peur
- "THE ONLY WAY"
- 70 / 30 %
- charte de design de l'app
- Human Design Guidelines
- Visicalc tableur
- cydia plateforme alternative
- rapport différent accéder au contenu du web à travers applications pas langages web libres


## Application

- pas de terminal, fichiers, dossier, etc.
- le programme devient un logo 
- l'icône de l'app est à la fois la rampe de lancement l'application et à la fois le dernier
  rempart vers ses fichiers nécessaires à son fonctionnement
- Ex: jeux vidéos sprites ou fichiers de config 

## Don Norman : une attitude de 

- exemple du boot
- exemple du moteurs
- exemple de l'UX sur son propre site
- Buisiness exists to serve people
- pas de citation de source

## Pourquoi le texte c'est moche

- Questionner les termes, les distinctions : Interface textuellee / Interface graphique
- Enseignement du graphique, rapport au texte
- Umberto Eco interface textuel : protestant (elitiste) / interface graphique : catho (populiste)
- Ex: Dwarf Fortress, avis des joueurs, 
- texte à l'écran perçut comme manque de design
- Texte métaphore au meme titre qu'un iĉone
- Lire la matrice
- Text based interface en Francais
- Habitude au GUI par défaut ou CLI
- Espace de travail par défaut = bureau virtuel
- Origine de l'interface graphique rapport à l'écrit

## Xerox : l'universel 

- Origine du terme Xerox (faire comme kodak, emprunt au grec, langage universel, bible et grec)
- Publicités sur l'accessibilité puis le frère Dominique (rapport à l'écrit biblique)
- Histoire de l'impression, diffusion des textes / lois à grande échelle / masse
- Réforme protestante arrêt de la messe en latin (universalité) / invention de l'imprimerie

## Frère Dominique, 1975

- Accessibilité Moyen age décrit comme période d'obscurantisme
- Clark,
- Weiser et l'exemple de l'encre et des scribes

## Origine du GUI chez Xerox

- Gui c'est le papier à l'écran
- Xerox Alto / Star
- WYSIWYG WYSIWYP
- modèle de l'imprimé
- détermination des interfaces
- origine pensée UX
- Ivan Sutherland, SketchPad
- Direct Interaction

## UX et Google Material Design

- Origine du matérial Design c'est le discours de l'UX qui se développe chez Google
- Enquête sur la Company Info 
- Description du material design, rapport au papier, accessibilité, métaphore
- Exemple d'enluminure biblique
- Otl Aicher 
- Précise jamais les designers dont ils s'inspirent
- Material Design Actuel Logo = logo du bauhaus
- Nouvelle Typograhie de Tschihold le sommaire décrit un interface

## Frère Dominique, 2017

- Diffusion de la bible à tout les peuples 
- Parallèle à ubiquité de l'informatique
- Unicode de Ted Nelson
- Modernes Otto Neurath Pictogrammes

## 10 commandements du design

- Jakob Nielsen
- Définir ds usages
- Synonymes de Good, etc.
