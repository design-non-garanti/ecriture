#Dénotation

## dispositif

Pc
Smart phone
Tablette
Mainframe
Cloud

## Programme

Boot
Application
DRM
PageRanking
Metafont
Latex
PDF
Word
Facebook
Autoshop

## produit

iPhone
iPad

## distributeur

Apple store 
Google play 

## entreprise

Xerox 
Apple 
Google 
Adobe

## Lieux

Xerox Parc
Pao Alto

## individu

Mark Weiser 
Jakob Nielsen 
Steve Jobs 
Don norman
Mathias Duartes
Donald Knuth
Jan tschichold
Olia Lialina
Guy Debord
Michel de Certeau
Ted Nelson
Larry Page
Sergey Brin
Sundar Pichai
Adrian Ward
Bill Gates
Donna Haraway

## notion

ordinateur invisible 
technologie invisible 
L'utilisateur comme victime
Detournement
Manières de faire
Stratégies
Tactiques
Tunning
Hack
DIY

## termes

user
people
computer
technology
interface
experience

## idéal

simplicité
facilité

## concept

Informatique ubiquitaire
Information appliance
Calm Technology
Internet of Things
Transparence

## courant de conception

User Experience 
User friendliness 

## environnements visuels sur écran

desktop
toolbox
table à dessin industriel
page
casse
matrice
dossiers
fichiers
site
GUI
TUI
CLI
îcone

## courant de design

Flat Design
Material Design
Skeuomorphisme
Constructivisme

## technologie

Informatique
Écriture
Internet
Imprimé

## ???

Propriétée
posséder 
louer

# Connotation