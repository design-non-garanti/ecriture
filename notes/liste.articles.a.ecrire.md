<!--
	# ensemble thématique d'articles
	## articles
	### sous parties d'articles
	*référence*
	>obet d'étude
-->

#Design comme dogme

#Brand God (la marque comme nouveau dieu)
*trouver les ref sur la marque comme nouveau dieu*

##Messe Evangéliste Apple
>iphone keynote 2007-01-09*

###Pomme dieu (logo, image)
>logo début de keynote

###Prédicateur Steve Jobs
> Pastor Joel Osteen's Full Sermon "The Power of 'I Am'" | Oprah’s Life Class | Oprah Winfrey Network, https://www.youtube.com/watch?v=_kjSK-PcU9o (comparer structurellement la iphone keynote avec un sermon du culte évangélique)
> Steve Jobs and the Goal of Preaching, https://www.9marks.org/article/steve-jobs-and-the-goal-of-preaching/ (un pasteur compare la position de Steve Jobs, celle d'anticiper les besoins des usagers avant *qu'eux même en ai conscience* avec celle de la bible qui ne fait pas que satisfaire *nos besoins*, qui les définit)

###Foule galvanisée

##Bible et ordinateur

###Reproduction et diffusion de l’écriture Xerox
>brother Dominic 2017

###Ordinateur et colonisation
>unicode de Ted Nelson

##Travail de bureau, travail de moine
>brother Dominic 1975
*tool or postproduction for the graphic designer*

#La surface comme système programmé

*Metahaven White Night Before A Manifesto May 2008*

> Software does precisely what its name spells out: it softens the relationship between man and manufacture. Writing, visiting friends, searching, finding, saving: what once required at least some physical activity becomes extremely light, pleasant and effortless. Such a soft regime presents itself as unconstrained and plural. While it seems to cross all territorial boundaries, software rather functions as escapism and synchronizes employment and pleasure over, and against, labour and life. It is because software presents itself as a neutral matter, as a non-directed and infinite open space, that the question of access and circulation in such an infinite void of potentiality arises. The spectacle of participation calls upon an undifferentiated behaviour where egalitarian enactments often smell like indifference and tend to obey rules of engagement that explicitly remain unwritten. Far from a no man’s land, the matrix of virtual tools, it independently establishes new relations and hierarchies among people, which are inevitably paired with impossibilities and hegemonies. 