# La page à l'écran

## La page et la fenêtre 

À l'écran notre environnement visuel est essentiellement constitué de pages et de fenêtres.
La métaphore du bureau est décrite comme une aide pour les utilisateurs pour interagir plus facilement avec l'ordinateur.

Il fut un temps où les bureaux étaient en réalité des bureaux

http://www.orilliapacket.com/2015/09/04/there-was-a-time-when-offices-were-actually-offices-and-the-technology-can-be-fascinating

Documents, fichiers, pages, cet environnement est celui du travail d'entreprise, bureau, comptable, administratif, économique, commercial, mercantile.
D'une certaine classe sociale, d'un certain héritage idéologique.

C'est l'environnement du travailleur à col blanc, du pouvoir (monastique)

Le moine copiste aussi utilise un bureau.

wikipédia, https://fr.wikipedia.org/wiki/Scribe :

>Un scribe est, au sens historique, une personne qui pratique l'écriture. Son activité consiste à écrire à la main des textes administratifs, religieux et juridiques ou des documents privés, et à en faire des copies. Il peut alors être assimilé à un copiste ou à un écrivain public.

wikipédia, https://fr.wikipedia.org/wiki/Copiste#En_imprimerie :

>Un copiste est un professionnel chargé de la reproduction de documents écrits ou d’œuvres d'arts. Ce métier est né de la nécessité de produire des copies de documents administratifs et de textes destinés à l'enseignement et à la propagation du savoir, bien avant l'invention de l'imprimerie, puis de la photographie et d'autres moyens de reproduction.

En 1991 Mark Weiser dans TCOT2C juge que l'ordinateur personnel comme technologie de l'information (malgré son succès commercial) a du mal à s’intégrer à l'environnement. Abordable uniquement par le biais d'un vocabulaire complexe très éloigné des tâches pour lesquels les gens l'utilise. Il compare cette situation avec la période durant laquelle les scribes devaient en savoir autant sur la fabrication de l'encre que sur l'écriture. Mark Weiser travail chez Xerox.

<!--

Pour la promotion de 

En 1975 Xerox diffuse une publicité mettant en scène un personnage nommé frêre dominique. 

Xerox “It's a Miracle” est un spot publicitaire datant de 1975.

Ce court film met en scène un personnage nommé frêre Dominique qui 

Dans cette publicité frêre Dominique -->

Xerox : “Monks”

créé par Allen Kay (à ne pas confondre avec Allan Kay) pour promouvoir la sortie du Xerox 9200 duplicating system, diffusé durant le super-bowl de 1977, frêre dominique devient une icône pour Xerox.

Le très court film s'ouvre dans un scriptorium sombre. Assis face à un bureau, frère Dominique à l'aide d'une plume, reproduit avec soin un ancien manuscrit.
Son environnement comporte lutrin, plumier, encrier et quelques feuilles dont ce qui semble être un *bavoir à encre* sont disposées inégalement sur la surface de la table. Contrairement aux représentations communes, la table ne comporte pas de pupitre ; Dominique dessine à plat sur la table.
Sur fond de ce qu'on identifie comme des chants grégoriens, le narrateur en anglais déclare 
>«Depuis que l'homme à commencé à enregistrer de l'information, il fut nécessaire de la reproduire»
<!--Every since people start to recording information there been a need to duplicate it   
-->
Durant cette narration la camera se rapproche du moine jusqu'à découvrir un plan à l'échelle de sa main et des caractères *gothiques* qu'il trace méticuleusement.
Le geste de sa plume semble d'une lenteur et d'une précision infinie, cet effet est accentué par un fondu enchaîné sur le visage du frère grimaçant et se massant l'*entre-yeux* de deux doigts. La tâche est si minutieuse qu'elle donne à Dominique des mots de tête. 

La pénombre, Le filtre sépia, le travail du décor, des costumes ajouté aux propos du narrateur donne le ton d'un certain type de reconstitution historique mettant en scène le moyen-age comme *l'âge sombre* une époque dystopique, mystique, perdue dans le temps abandonnée par la raison et la science proche de l'univers <!--visuel--> du *nom de la rose*. Le jeu d'acteur simple et archétypal contraste avec la lourdeur de la reconstitution tout en renforçant la bêtise ou naïveté attribué aux homme du moyen age ainsi représenté.

Son œuvre achevée, il présente avec fierté son travail à l'abbé. Ce dernier se trouve assis devant un bureau à la configuration similaire, plus luxueux dans une pièce modeste, lumineuse et meublée. Tandis que nous sommes toujours accompagnés de chants grégoriens. En examinant attentivement le manuscrit qui lui a été présenté, l'abbé félicite généreusement Dominique pour la qualité de son travail. C'est avec un entrain à peine dissimulé qu'il fini par demander au scribe 500 copies supplémentaires.

Retournant sur ses pas, le sourire qui habitait le visage de frère Dominique s'estompe instantanément à la seconde il tourne le dos à son supérieur. L'instant suivant alors qu'il revenait penaud sur ses pas traversant le *cloitre* une idée géniale semble lui traverser l'esprit, il rebrousse chemin et s'empresse de passer une lourde porte métallique s'ouvrant sur une lumière blanche surnaturelle.
En passant cette porte frère Dominique se retrouve instantanément dans un magasin de copies *flambant neuf* de la fin du XXe 1975 (contemporain à la sortie du spot). Et les choeurs que l'on entendait continuellement en fond sonore se coupent.

Cette fois, l’environnement est très lumineux. Toujours peu coloré les tons passent du marron noir orange au bleu blanc et beige.

Dominique est reconnu immédiatement, tandis qu'une employée *printer girl* lui tiens la porte en souriant, un employé modèle, sosie de Clark Kent l’accueil chaleureusement un fichier à la main.
>«Hey ! Frère Dominique comment ça va?».

Le plan se ressert sur les deux personnages et l'on remarque clairement que l'employé dépasse le frère Dominique d'environ une tête et demi. 

Dominique lui demande en lui tenant amicalement l'épaule :

>«Peux-tu faire un grand travail pour moi ?»

Tandis que les deux personnages se dirigent vers une machine occupant environ un tiers de la pièce, on peux observer une armoire, contenant des encres et des ramettes de papier, des fiches attachées à des tableaux en liège accrochés aux murs, des plantes et des bureaux dans des pièces voisines, des employés s'activant autour des deux protagonistes, en pleine discussion ou transportant des dossiers sur un *caddie*.

Dans ce mouvement tandis le duo entame une discussion le son se coupe et le narrateur commente 

«le Xerox 9200 duplicating system contrairement à tout ce que nous avons jamais fait, scan et tri automatiquement des originaux grâce à un ordinateur intégré qui contrôle l'ensemble du système et peut dupliquer réduire et assembler un nombre pratiquement illimité des documents complets et le fait à une incroyable vitesse de deux pages par seconde»

the Xerox 9200 duplicating system unlike anything we've ever made feeds and cycles originals has a
computerized programmer that controls the entire system can duplicate reduce
and assemble a virtually limitless number of complete sets and does it all at an incredible two pages per second

arias Ed's father 500 sets you asked for it's a miracle

le système de duplication Xerox 9200 contrairement à tout ce que nous avons jamais fait des flux et des originaux de cycles a un
programmateur informatisé qui contrôle l'ensemble du système peut dupliquer réduire
et assembler un nombre pratiquement illimité d'ensembles complets et le fait à une vitesse incroyable de deux pages par seconde.

Le père de arias Ed 500 ensembles vous avez demandé c'est un miracle

Frère Dominic finit avec soin et de reproduire un vieux manuscrit pour apprendre que le chef moine a besoin de 500 autres ensembles. Dominic se dirige par une porte secrète vers un magasin de copie moderne où la Xerox 9200 (qui peut copier à un taux incroyable de deux pages par seconde!) Fait le travail pour lui. Il retourne au monastère et livre les décors en un rien de temps. "C'est un miracle", dit le père. Le frère Dominic sourit vers le ciel.

1 DigiBarn TV Features: 
"Xerox Monks" Classic Superbowl ad for Xerox 9200 Duplicating System 
and other commercials in the "Xerox Monks" series by Allen Kay http://www.digibarn.com/collections/movies/digibarn-tv/xerox-monks/index.html



<!-- lecture commentée de OUTIL (ou, le designer graphique face à la post-production)andrew blauvelt, Traduit de l’américain par Véronique Rancurel (révision Samuel Vermeil) -->



#premier paragraphe p3

>Ed Fella … fait allusion à un collage … récréant ainsi le plan de travail d’un designer graphique type, aux environs de 1975.

>Aujourd'hui le bureau type est virtuel.

On peux voir cette détermination comme le résultat d'un *chiasme || sophisme* conséquent à l'informatique ubiquitaire : si l'ordinateur est partout, tout est ordinateur et c'est pour cela que le bureau type (virtuel) est celui de l'ordinateur. Autre point intéressant, sous-entendu dans le paragraphe : le plan de travail type est le bureau. D'où vient le bureau ? <Travail de bureau, travail de moine>

*(There was a time when offices were actually offices, and the technology can be fascinating).*

>Inventé au sein des célèbres entreprises Xerox Park & Apple computer 

*(voir xerox alto et apple star)*

>Rien n’illustre mieux la transformation du design graphique que cette mutation métaphorique de son espace de travail 

La métaphore ne se limite pas au bureau et ces mutations ne concernent pas uniquement l'espace de travail du designer graphique. Les dossier et fichiers sont une métaphore (Unix voir Ted Nelson « we don't examine this very often because it is assumed that the world is made merely of data lumps »), la page, la fenêtre, le bureau (Xerox, Apple), le papier (Material Design), ces métaphores déterminent l'ordinateur contemporain. Malgré leur relative diversité ces mutations métaphoriques tendent systématiquement vers le monde technocratique, bureaucratique, celui des «travailleurs en col blanc». 
Machine Turing-complet, l’ordinateur est capable de représenter toutes les fonctions calculables. Cet état induit une omnipotence figurative/simulatrice donc une non-détermination fondamentale 

c'est ainsi un outil capable de supporter n'importe quelle activité,
c'est peut-être pour cela qu'il devient ubiquitaire ?

Ainsi l'ensemble des utilisateurs quel que soit leur activité ou leurs aspirations travaillent avec un ordinateur.

C'est la bureaucratisation de l'ensemble des activités et en même l'ordinateur est utiliser comme un atelier

c'est un outil permettant de simuler potentiellement toute 
Propriétaires ou libres les logiciels  
>le designer a perdu sa dimension artistique pour devenir métier de technocrate et il est passé du statut d’emploi qualifié à celui de prestation de service.

Si le bureau est l'environnement du travailleur en col blanc, le designer travaillant avec un bureau à t-il été un jour autre chose qu'un prestataire de service ? <Travail de bureau, travail de moine>

Nouvelle transformation du designer graphique en cours, expérimentée chez les designers-développeurs designers graphiques intervenant dans la production “d'apps”. Avec la multiplication des méthodes de travail “workflow” méthodes Agile, Scrum et Kanban souvent dérivées du Toyotisme il passe du statut de prestataire de service à celui du prolétaire. Cette transformation s'accompagne peut-être de la disparition du PC ? Quel plan de travail correspond à cette nouvelle transformation ?

#deuxième paragraphe p3

>Ce qui est supposé distinguer les humains de leurs ancêtres primates, c’est qu’ils ne se contentent pas d’utiliser les outils mais qu’ils cherchent à les intégrer à leurs activités quotidiennes, à leur découvrir de nouvelles fonctions et à imaginer de nouveaux usages.Cette séquence évolutive a été immortalisée sur grand écran par Stanley Kubrick dans 2001, l’Odyssée de l’espace (1968)

#Thématique de la surface 

##p9

>…Virginia Postrel, qui plaide pour la grandeur et les vertus de l’esthétique, du style et de la surface dans un âge dominé par le visuel : « Cette nouvelle ère est un défi pour nous tous — designers, ingénieurs, dirigeants d’entreprises, et pour le public tout entier — défi qui nous enjoint à penser différemment la relation entre surface et substance, esthétique et valeur. Les designers ont longtemps vécu dans la peur que les gens les croient frivoles et qu’ils considèrent leur travail comme « joli, mais stupide », dénigrant leur expertise durement acquise, et  les mettant en première ligne pour les coupes budgétaires. quasiment toutes les définitions du design commencent par énoncer avec emphase que le métier n’est pas simplement une affaire de surfaces »  9. Mais, bien sûr que les graphistes sont des producteurs de surfaces, des millions de surfaces. Cette production est si vaste qu’elle se compare aux immeubles en dur. Selon Metahaven : « En design, la production de surface équivaut à la production d’espace 10.

#La surface comme système programmé :

<!--
L'autre coté de l'ordinateur 
Même si cette définition est contestable Que ce qui constitue les ordinateurs soient une suite de 0 et de 1 
Depuis *Matrix* il est communément admis que l'ordinateur 
Le film Matrix décrit le monde de l'ordinateur comme un monde d'illusions fabriquées

Le fait que ce qui nous est donné à voir de notre ordinateur personnel est un assemblage de surfaces

Si la surface à l'écran est une métaphore, une simulation peut-on encore considérer le designer graphique sur ordinateur comme un concepteur de surface ?

Le designer graphique qui produit et publie sur écran 

Cet *éclatement* de la *nature* de la surface comme système programmé (sur un ordinateur tout est programmé) questionne la conception 
-->


>Dans son acception la plus large, le design de la post-production peut aller jusqu’à englober le design d’objets préexistants mais vierges : sacs fourre-tout, papier peint, tee-shirts, boutons, assiettes, affiches, tasses à café, etc. C’est la projection du graphisme sur tout type de surface. Ce n’est pas par hasard que ces surfaces tendent à être les formes commerciales du design lui-même — formats pratiques et disponibles, coques vides qui attendent d’être remplies — connues sur le marché sous le dénominatif affectueux de « merch ».