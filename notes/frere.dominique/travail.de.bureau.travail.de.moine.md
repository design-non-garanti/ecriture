# Pourquoi le texte est-il moche ?

Pour nous autre Designers graphiques, dans le cadre du design graphique en tant que discipline, culture.

La distinction commune entre interface en lignes de commandes (CLI, command line interface), et interface graphique (GUI, graphical user interface) perd du sens, peut paraître absurde ou mal placée. 

La typographie étant largement enseignée  dans les section design graphique, communication visuelle dans et en dehors des beaux-arts.
Quel que soit la définition que l'on donne du designer graphique, qu'il s'agisse d'un producteur de surfaces ou du responsable de la mise en forme de l'information, le texte est graphique.

À partir du moment où un caractère ou un glyphe est considéré comme élément graphique.

Graphique viens du grecque ancien graphis et signifie écrire.

Écrire selon le Larousse signifie 
>Former les lettres, les signes, avoir tel ou tel type d'écriture, employer tel ou tel système d'écriture

Pourtant sur écran, un système composé de lettres, du texte n'est pas graphique, et n'est peut-être pas non plus une interface: exemple : l'acronyme TUI (text-based user interface) n'ayant aucun équivalent dans la langue française. 

Derrière cette distinction se cache celle de l'opposition image - texte, catholique - protestant.
Selon Umberto Eco – 

back-page column, "La bustina di Minerva," in the weekly Espresso (30 September 1994).
english : http://jowett.web.cern.ch/jowett/EcoMACDOS.htm
français : https://www.coindeweb.net/humour/info_eco.html

Une certaine argumentation dénonce la tromperie exercée sur l'utilisateur par les interfaces dites graphiques (sans pour autant contester cette appellation de graphique) cette distinction est celle de la métaphore ou de son absence, le texte étant proche d'une hypothétique vérité de la machine tandis que les îcones, le glissement, les fenêtres userait de métaphores éloignant l'utilisateur de ce qui se passe vraiment dans l'ordinateur.

Florian Cramer dans Command Line Poetics désamorce cet argument en rapellant que les lignes de commandes sont aussi des métaphores au sens de Mc Luhan des déplacement d'un ancien médium dans un nouveau, par exemple les commandes UNIX BSD, Linux/GNU et MacOSX emulent le fonctionnement des machines télétype qui servaient pour terminaux dans le début des années 70
*voir florian cramer Command Line Poetics*.

Cette division texte complexité, image simplicité se retrouve dans le jeu vidéo.

Le terme graphismes dans un jeu vidéo désigne spécifiquement ce qui nous est donné à voir du jeu et l'utilisation de texte comme interface utilisateur (Les jeux qui utilisent des graphismes en ASCII par exemple – l'ASCII norme de codage de caractère ).
Dénote un manque de graphisme, des mauvais graphismes, une absence volontaire ou non de considération ésthétique, un graphisme sale. Sale car peu intuitif, par opposition propre car intuitif.

Exemple Dwarf fortress, un jeu de simulation de *civilisation* les graphismes sont un problème récurrent, souvent remplacés modables.

>I know that this has been asked before, but this post is part question, part rant. Let me start by saying that I LOVE the idea behind this game. It's just so..... FUN!
The thing is, the graphics make my eyes bleed, not just the ascii ones, even the tilesets like spacefox are tiring to the eye. I know that stonesense exists and provides an isometric view in the game, but the graphics there are even worse than tilesets; everything just seems so....ugly. I am not one to really care about graphics, but I do draw a line somewhere.
Then there's the case of the GUI being difficult to use and not so intuitive. I really do long for a rimworld style GUI.
So, I ask you fellow redditors and DF players, jave you encountered a mod that provides clean graphics in an isometric view (hell, even top down view) that works seamlessly with the game and has a better GUI (more intuitive, less tiring)? I want to play the FUCK out of this game, but without visual stimulus, the things I will build will just not feel that good.
Dwarf Fortress with better graphics on reddit, https://www.reddit.com/r/dwarffortress/comments/5ohf67/dwarf_fortress_with_better_graphics/?st=jao00ooq&sh=3c5e9d68

Sinon dénote un graphisme retro, *old school* le text sur écran d'ordinateur étant effectivement le paradigme type dans les premières périodes de l'histoire de l'informatique.

Pourtant cette dichotomie texte complexité, image simplicité semble se renverser avec l'habitude 

>Honestly? I find the ASCII to look quite decent. Maybe I've been playing the game for too long, though.
At first I couldn't make heads or tales out of the ASCII but after learning the game through tilesets I started appreciating the aesthetic value of the ASCII, things fit together nicely whereas tilesets always have that one or two weird tiles that stand out and don't fit.
Also, if you want a DF that's horribly broken but in 3d, you can always try to find the original Armok God of Blood. It's the precursor to Darf Fortress. Think adventure mode in 3d (with more bugs then all of fortress and adventurer mode combined)
Also, I think it's near impossible at the moment do do a decent UI. Smarter people then me can correct me on this, but accessing everything the game has to offer is simply not possible from outside the game.
Try out (square) ASCII, somehow my forts there look cleaner then in tilesets, because things fit together and nothing is especially distracting.
Forget about graphics, aesthetics is where it's at.
Dwarf Fortress with better graphics on reddit, https://www.reddit.com/r/dwarffortress/comments/5ohf67/dwarf_fortress_with_better_graphics/?st=jao00ooq&sh=3c5e9d68

Yeah, those of us who have learned to look at a DF screen and see whats there despite the ASCII are in a unique situation.
I was playing DF while travel by train a couple years back. I caught myself worrying if any other passengers were thinking I was crazy with all the blood and bones on screen. Then I remembered they most likely couldn't even tell I was playing a game.
https://www.reddit.com/r/dwarffortress/comments/26f71s/i_i_dont_even_see_the_ascii_anymore/?st=jar0pa6u&sh=80d9dc90

L'utilisation de raccourcis dans un jeu et plus généralement dans un logiciel amène à une simplicité innatendue, comme acquérir *la capacité de comprendre la matrice (trouver exemple)* le problème semble être celui de l'habitude.
Les joueurs de dwarf une fois initiés à la matrice décrivent l'ésthétique du jeu comme limpide immersive très narrative permettant de prendre en compte de très grandes quantitées d'information en un clin d'oeil.

>Cypher - Whoa, Neo. You scared the bejeezus out of me.
>Neo - Sorry.
>Cypher - It's okay.
>Neo - Is that...
>Cypher - The Matrix? Yeah.
>Neo - Do you always look at it encoded?
>Cypher - Well you have to. The image translators work for the construct program. But there's way too much information to decode the Matrix. You get used to it. I... I don't even see the code. All I see is blonde, brunette, red-head. Hey, you a... want a drink?
>Neo - Sure.

>Le joueur néophyte pourra être récalcitrant compte tenu de la charte graphique résolument rétro, et de l'interface maladroite du jeu. Pourtant, derrière ce côté Old School assumé se cache un bijou rare; La profondeur du gameplay, la grande immersion, la liberté totale du joueur et la richesse de l'univers compensent largement cette phase d'apprentissage ardue.
Le Wiki francophone de Dwarf Fortress, http://www.dwarffortress.fr/wiki/index.php?title=Accueil

Le problème est donc celui de l'habitude et de l'idéologie. Mais quelles sont nos habitudes et d'où viennent t'elles ? 

La question de l'habitude prends une importance considérable dans notre ère d'informatique ubiquitaire 

Dans Outil ou, le designer graphique face à la post-production, est introduit par le bureau virtuel comme environnement de travail type du designer graphique d'aujourd'hui.

La métaphore ne se limite pas au bureau et ces mutations ne concernent pas uniquement l'espace de travail du designer graphique. Les dossier et fichiers sont une métaphore (Unix voir Ted Nelson « we don't examine this very often because it is assumed that the world is made merely of data lumps »), la page, la fenêtre, le bureau (Xerox, Apple), le papier (Material Design), ces métaphores déterminent l'ordinateur contemporain. Malgré leur relative diversité ces mutations métaphoriques tendent systématiquement vers le monde technocratique, bureaucratique, celui des «travailleurs en col blanc». 

On peux voir cette détermination comme le résultat d'un *chiasme || sophisme* conséquent à l'informatique ubiquitaire : si l'ordinateur est partout, tout est ordinateur et c'est pour cela que le bureau type (virtuel) est celui de l'ordinateur. Autre point intéressant, sous-entendu dans le paragraphe : le plan de travail type est le bureau. D'où vient le bureau ? <Travail de bureau, travail de moine>

#

Si les interfaces dites 'graphiques' s'opposent aux interfaces textuelles il est assez étonnant de constater que toutes deux proviennent de la culture de l'imprimé. Et plus précisément de la question de la diffusion de masse, diffusion de l'écriture propre à l'imprimé. 

On parle beaucoup de la mort du livre et de l'auteur à cause de l'ordinateur mais le livre dans son rôle *originel* – la diffusion du savoir, de la bonne parole – est lin d'être mort bien au contraire, tandis que l'auteur en tant que responsable d'une publication n'a jamais été aussi identifiable, la diffusion de *l'écriture sainte* – c'est à dire une écriture qui ne se contente pas de satisfaire nos besoins mais qui les définit – ne s'est jamais aussi bien porté ou est à l'origine de l'ordinateur tel que nous le connaissons aujourd'hui
*voir max weber l'éthique protestante et le capitalsime?*

    from : Eva Hemmungs, No Trespassing Authorship - Pál Selényi https://en.wikipedia.org/wiki/P%C3%A1l_Sel%C3%A9nyi  xerography https://en.wikipedia.org/wiki/Xerography

Entre 1935 et 1938 Chester Carlson s’intéresse à la photoconductivité et particulièrement à un article de l'ingénieur Pál Selényi (Tungsram entreprise hongroise d'ampoules et électronique), après plusieurs expérimentations ratées, quelques conseils de son épouse, l'utilisation d'un local appartenant à sa belle-mère et l'aide d'un physicien allemand réfugié, il fini par mettre au point un procédé d'impression qu'il nomme électrophotographie. D'abord, il faut écrire à l'encre sur une lame de verre, puis il faut frotter une plaque métallique recouverte de soufre avec un chiffon pour lui donner une charge électrique. Il faut placer la glissière contre la plaque, placer l'ensemble sous une lampe puissante pendant quelques secondes, enlever la glissière et saupoudrer du pigment sur la plaque. L'inscription apparaît. Cette technique est  celle des imprimantes laser, LED et employée  dans la plus part des photocopieuses et le pigment saupoudré sur la plaque est appelée toner. Carlson tente ensuite d'obtenir sans succès des financements de la part d'IBM, Kodak, General Electric et de RCA pour développer son invention.
C'est par le biais d'une association à but non-lucrative qui finit par obtenir un accord avec une entreprise spécialisée dans les services et produits photographiques nommée Haloid en 1947. En 1948 l'électrophotographie est renommée Xerographie du grecque *xeros* pour sec et *graphis* du latin pour écriture, terme plus *intuitif* proche d'une description du procédé de Carlson et le terme Xerox deviens une marque déposée. En 1956 l'ensemble des droits sur les brevets xérographiques sont conférés à Haloid et en 1958 Haloid change officiellement de nom pour Haloid Xerox puis Xerox en 1961 – inspiré par le fait que le nom Kodak est construit autour de la même première et dernière lettre.
Dans le magazine fortune en 1961 Xerox insiste sur le fait qu'il n'y a rien d'ancien et de grecque dans l'esprit de l'entreprise soulignant plutôt son engagement à répondre aux exigences de la diffusion de masse de l'information.

Dire qu'il n'y a rien de grec et d'ancien dans Xerox est faux car il y a tout de l'universalité occidentale.

>Emmanuel Lévinas écrit: «Qu'est-ce que l'Europe? C'est la Bible et les Grecs. [...]  Il faut, par l'amour de l'unique, renoncer à l'unique. Il faut que l'humanité de l'Humain se replace dans l'horizon de l'Universel. Ο messages bienvenus de la Grèce! S'instruire chez les Grecs et apprendre leur verbe et leur sagesse»2. 
    À l'heure des nations, Paris, éditions de Minuit, 1988, p. 155-156. dans Vieillard-Baron Jean-Louis. Hegel et la Grèce. In: Le Romantisme et la Grèce. Actes du 4ème colloque de la Villa Kérylos à Beaulieu-sur-Mer du 30 septembre au 3 octobre 1993. Paris : Académie des Inscriptions et Belles-Lettres, 1994. pp. 55-61.

En ce sens, la Grèce est bien l'universalité de l'Occident. 

Kodak est un Mot créé arbitrairement en 1888 pour ses possibilités internationales d’emprunt par George Eastman (1854-1932) qui le déposa comme nom pour son appareil photographique portatif.
https://fr.wiktionary.org/wiki/kodak#en 

Car Xerox est marqué par la reproduction et sa diffusion, l'accessibilité et les références bibliques.

La publicité de xerox stigmatise cette pensée universelle occidentale


C'est au Xerox Park que 
Xerox est une entreprise qui vend des imprimantes avant tout et c'est dans cette 

Conçu au Xerox PARC en 1973, Xerox Alto est l'un des premiers ordinateur personnels PC, c'est également le premier ordinateur à comporter une interface dite graphique et un environnement de bureau.



 interface textuelle (TUI)