En 1991 l'informaticien Mark Weiser, publie l'article phare *The Computer for the 21st Century*[^1]
portant sur le concept d'informatique ubiquitaire (Ubiquitous Computing).  Weiser débute son article
en soutenant que « Les plus profondes technologies sont celles qui disparaissent.  Elles se lient
dans le tissu de la vie quotidienne jusqu'à ce qu'elles ne se distinguent pas ». Pour appuyer cette
idée Weiser prend comme exemple l'omni-présence de l'écriture au travers des livres, des enseignes
de rue, des emballages de bonbons, etc. Selon lui « la présence constante de ces produits de
technologie littéraire ne nécessite pas d'attention active » : lorsque nous regardons des panneaux
de signalisation nous absorbons l'information sans effectuer « conscieusement l'acte de lire ». 
