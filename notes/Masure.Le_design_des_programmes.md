---
title: Le design des programmes
author: Masure, Anthony
date: 2015
lang: FR
type: book, thesis
---

# Le design des programmes

## Braun / Apple, des survivances paradoxales

- Design envisagé simplement comme production d'objet nouveaux.
- Nouveauté étant comprise comme rupture formelle avec esthétiques admises.
- Centré sur l'apparence non la structure.

### Une morale de la forme

- Jonathan Ive (en charge du département design chez Apple), expose sa volonté de
  faire des objets plus discrets et moins encombrants.
- Plus léger, plus rapide, plus simple.
- Objet envisagé dans une logique de la performance.
- Erwin Braun et la métaphore du majordome anglais.
- Dans quoi s'inscrit Braun et les différences avec Apple.
- Standardisation entraîne absence de jeu (divergence refusée)

### Un modernisme obsolecent

- Formes développées par Dieter Rams forme un langage, malgré volonté de neutralité.
- L’objet Braun se fige dans une expression de la neutralité. Ayant traversé l’époque pré- numérique
  durant laquelle nous avons vieilli ou grandi, les productions Braun incarnent ce passage de la
  technique aux « nouvelles » technologies. Nous distinguons ici la tech- nique, qui désigne un mode
  d’accès aux choses directement lié au geste et donc au corps, de la technologie, pensée visant à
  l’établissement de méthodes scientifiques, c’est-à-dire nommables, réutilisables et identifiables.
- L'Apple I (1976) est livré avec ses plans de montage. Sa structure en bois et son mode de
  commercialisation valorisent l'idée d'un objet ouvert, appropriable et reproductible. Langage de
  programmation adaptable et documenté (le BASIC), ports de branchement pour cartes et lecteurs
  externes. Utilisateur vu comme co-créateur. Public visé plus par ses compétences que par ses
  besoins. 
- L'Apple II (1977) va se développer autour du constat que peu de personnes se saisissent du mode
  d'emploi et des possibilités d'ouverture de l'Aplle I. Steve Jobs: vers objets nécessitant moins
  d'efforts.
- iMac G3 intègre dans le même boitier l'unité centrale et l'écran ne permet pas de rajouter des
  composants internes tels que RAM (corrigé par la suite).
- Par la suite, batteries inamovibles, Apple Store rigide et censuré, OS propriétaire, connectique
  non-standard, etc.
- On peut identifié une tendance vers la restriction chez Apple.
- En cachant les indicateurs non-nécessaires et en cloisonnant les éléments techniques, les produits
  Apple donnent l’illusion d’une mono-fonctionnalité dans une typologie de produit perçue
  habituellement comme confuse et compliquée.
- Apps, petits carés clos sur des usages prévisibles.
- Braun: G11, SK2, SK4, T3, etc. et Apple: iPhone1, 2, 3, 4, etc. Numérotation croissante dévalorise
  le chiffre inférieur.
- Incompatibilité avec les anciens modèles.
- L'impossibilité de lire un document dans un format récent pousse l'utilisateur à se mettre à jour.
- Au fond, ces pratiques qui dépassent largement le cas d’Apple nous interrogent sur des façons de
  faire du design qui ne cher- cheraient pas à masquer leur intentions sous des formes en
  inadéquation avec les dis- cours développés.

### Mythologies technologiques

- ...

### Une injonction paradoxale

- Les guidelines de communication de Braun mettent l'accent sur les qualités intrinsèques des
  produits, ne pas détourner l'attention du lecteur : refus de slogans, prases chocs, corps
  séduitsants, etc. Honnêté du discours.
- Programme Ulmien construit en réaction à un contexte économique et politique totalitaire
- Au contraire Apple ne contestent pas les structures de pensées de leur époque
- De Braun à Apple, on passe d’une consommation responsable et transparente (au sens propre, comme
  le tourne-disque sk4), à une capitalisation accélérée avançant masquée sous couvert de valeurs
  alternatives.
- Jobs reprent la direction d'Apple en 1997
- éloigne l'attention portée aux objets au profit d'une mystique d'entreprise
- « Think different » pas rupture avec le passé mais inscription de la créativité
- Reprendre les formes de Braun c'est ne rien contester 
- Jobs dits que art et technologie ne sont pa séparés pourtant on peut questionner son adéquation
  avec les objets réalisés.
- Penser autrement sans la possibilité de modifier son appareil
- Un même message diffusé en masse (c’est-à-dire un message publicitaire) qui demande à « penser
  différemment » peut être qualifié d’« injonction paradoxale ».
- « Think different » faconner des comportements qui déterminent des façons de penser. Orienter pour
  faire penser différemment.
- Comment penser différemment alors qu'il est impossible de modifier ces objets ?

### Le silence du design

- ...
